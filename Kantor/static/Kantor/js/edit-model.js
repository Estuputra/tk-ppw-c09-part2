$(document).ready(function () {
    $('.closebtn').on('click', function () {
        $('.alert').css('opacity', '0');
    });

    $('#btnJobseeker').on('click', function (e) {
        $('.alert').css('opacity', '0');
        $.ajax({
            type: 'POST',
            url: "/Kantor/edit-profile/",
            data: {
                nama_perusahaan: $('#id_nama_perusahaan').val(),
                alamat: $('#id_alamat').val(),
                desc: $('#id_desc').val(),
                csrfmiddlewaretoken: $('input[name = csrfmiddlewaretoken]').val(),
            },
            success: function (response) {
                $('.alert').css('opacity', '1');
                $('#judul-perusahaan').html("Edit profil perusahaan <b>" 
                        + $('#id_nama_perusahaan').val() +"</b>");
            },
        });
    });
});