from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from django.contrib.auth.models import User
from Kantor.models import Kantor, KantorUser
from Kantor.forms import FormKantor

from Kantor.apps import KantorConfig
from Jobseeker.models import Jobseeker
from django.contrib.auth.models import Group


# Create your tests here.
class ModelTestKantor(TestCase):
		def setUp(self):
				self.pembuat_lowongan = KantorUser.objects.create(
						nama="PT Sejahtera",
						alamat="Kuburan",
						deskripsi_kantor="kantor"
				)
		def test_instance_created(self):
				self.assertEqual(KantorUser.objects.count(), 1)
				self.assertEqual(str(self.pembuat_lowongan), "PT Sejahtera")

		def test_buat_lowongan(self):
			lowongan = Kantor.objects.create(
				Kantor = self.pembuat_lowongan,
				nama_pekerjaan="memacul",
				alamat="sawah",
				deskripsi_pekerjaan="oke"
			)
			self.assertEqual(Kantor.objects.count(), 1)    	
			self.assertEqual(str(lowongan), "memacul")

		
class UnitTest(TestCase):
		#Tes landing page Kantor
	def test_template_landing_page_kantor(self):
		response = self.client.get('/Kantor/')
		self.assertTemplateUsed(response, 'Landing-Kantor-User/landing.html')

		#Tes landing menggunakan user bukan Kantor
	def test_landing_page_kantor(self):
		response_register = self.client.post(reverse("register"), follow=True, data={
												"username": "jaaa",
												"email": "email@gmail.com",
												"password1": "admin123",
												"password2": "admin123",
												"typePekerjaan": 'Jobseeker'
										})

		response_login = self.client.post(reverse("login"), follow=True, data={
												"username": "jaaa",
												"password": "admin123"
										})

		response_lading_page = self.client.get(reverse('landing-kantor'))
		self.assertEqual(response_lading_page.status_code, 302)

		#Tes Halaman registrasi kantor
	def test_register_form(self):
		response = self.client.get('/Kantor/register/')
		self.assertTemplateUsed(response, 'Kantor/login/register-kantor.html')

		#Tes registrasi Kantor
	def test_register(self):
		response1 = self.client.post(reverse("register-kantor"), follow=True, data={
												"username": "jaaa",
												"email": "email@gmail.com",
												"password1": "admin123",
												"password2": "admin123"
										})
		
		count = User.objects.count()
		self.assertEqual(count, 1)

	def test_register_user_already_existed(self):
		response1 = self.client.post(reverse("register-kantor"), follow=True, data={
												"username": "jaaa",
												"email": "email@gmail.com",
												"password1": "admin123",
												"password2": "admin123"
										})

		response2 = self.client.post(reverse("register-kantor"), follow=True, data={
												"username": "jaaa",
												"email": "email@gmail.com",
												"password1": "admin123",
												"password2": "admin123"
										})
		self.assertEqual(response2.status_code, 200)

	def test_register_false_password(self):
		response = self.client.post(reverse("register-kantor"), follow=True, data={
												"username": "jaaa",
												"email": "email@gmail.com",
												"password1": "admin123",
												"password2": "admin"
										})

		self.assertEqual(response.status_code, 200)

		#Tes login kantor
	def test_register_is_authenticated(self):
		response_register = self.client.post(reverse("register-kantor"), follow=True, data={
												"username": "jaaa",
												"email": "email@gmail.com",
												"password1": "admin123",
												"password2": "admin123"
										})

		response_login = self.client.post(reverse("login"), follow=True, data={
												"username": "jaaa",
												"password": "admin123"
										})

		response_logined = self.client.get(reverse("register-kantor"))

		self.assertEqual(response_logined.status_code, 302)

		#Tes List-lowongan General buat Pelamar
	def test_url_listLowongan(self):
		response = Client().get(reverse("list-lowongan"))
		self.assertEqual(response.status_code, 200)

	#Tes List-lowongan General buat Perusahaan
	def test_url_listKantor_Kantor(self):
		response_register = self.client.post(reverse("register-kantor"), follow=True, data={
												"username": "jaaa",
												"email": "email@gmail.com",
												"password1": "admin123",
												"password2": "admin123"
										})

		response_login = self.client.post(reverse("login"), follow=True, data={
												"username": "jaaa",
												"password": "admin123"
										})

		response = self.client.get(reverse("list-lowongan"))
		self.assertEqual(response.status_code, 200)

	#Tes List-lowongan General buat JobSeeker
	def test_url_listKantor_JS(self):
		response_register = self.client.post(reverse("register"), follow=True, data={
												"username": "haaa",
												"email": "email@gmail.com",
												"password1": "admin123",
												"password2": "admin123",
												"typePekerjaan": 'Jobseeker'
										})

		response_login = self.client.post(reverse("login"), follow=True, data={
												"username": "haaa",
												"password": "admin123"
										})

		response = self.client.get(reverse("list-lowongan"))
		self.assertEqual(response.status_code, 200)	

		#Tes Edit kantor setelah login
	def test_edit_kantor(self):
		response_register = self.client.post(reverse("register-kantor"), follow=True, data={
												"username": "jaaa",
												"email": "email@gmail.com",
												"password1": "admin123",
												"password2": "admin123"
												})

		response_login = self.client.post(reverse("login"), follow=True, data={
												"username": "jaaa",
												"password": "admin123"
												})

		response_edit = self.client.get(reverse("editProfileKantor"))
		response_post_edit = self.client.post(reverse("editProfileKantor"), follow=True, data={
												"nama_perusahaan": "ganti",
												"alamat": "rumah",
												"desc":"perusahaan keren"
												})
														
		self.assertEqual(response_register.status_code, 200)
		self.assertEqual(response_login.status_code, 200)
		self.assertEqual(response_edit.status_code, 200)
		self.assertEqual(response_post_edit.status_code, 200)   

		#Tes page tambah lowongan belum login
	def test_tambah_lowongan_for_this_kantor(self):
		response_register = self.client.get(reverse('tambah-lowongan'))
		self.assertEqual(response_register.status_code, 302)

		#Tes page tambah lowongan setelah login
	def test_get_lowongan_is_authenticated(self):
		response_register = self.client.post(reverse("register-kantor"),data={
												"username": "jaaa",
												"email": "email3@gmail.com",
												"password1": "admin123",
												"password2": "admin123"
				})

		response_login = self.client.post(reverse("login"), data={
												"username": "jaaa",
												"password": "admin123"
				})

		response = self.client.post(reverse("tambah-lowongan"), data={
												"nama_pekerjaan": "macul",
												"alamat":"jalan",
												"tipe_pekerjaan":"Fulltime",
												"desc":"ini pekerjaan" 
				})
		
		response_tambah_lowongan = self.client.get(reverse('tambah-lowongan'))
		self.assertEqual(response_tambah_lowongan.status_code, 200)

		#Tes page tambah lowongan setelah login
	def test_post_lowongan_is_authenticated(self):
		response_register = self.client.post(reverse("register-kantor"),data={
												"username": "jaaa",
												"email": "email3@gmail.com",
												"password1": "admin123",
												"password2": "admin123"
				})

		response_login = self.client.post(reverse("login"), data={
												"username": "jaaa",
												"password": "admin123"
				})

		response = self.client.post(reverse("tambah-lowongan"), data={
												"nama_pekerjaan": "macul",
												"alamat":"jalan",
												"tipe_pekerjaan":"Fulltime",
												"desc":"ini pekerjaan" 
				})
		page_list_lowongan_this_kantor = self.client.get(reverse('this-applicants'))
		self.assertEqual(page_list_lowongan_this_kantor.status_code, 200)
		self.assertEqual(response.status_code, 302)

	def test_lihat_detail_lowongan_KantorUser(self):
		response_register = self.client.post(reverse("register-kantor"),data={
												"username": "jaaa",
												"email": "email3@gmail.com",
												"password1": "admin123",
												"password2": "admin123"
				})

		response_login = self.client.post(reverse("login"), data={
												"username": "jaaa",
												"password": "admin123"
				})

		response_tambah_lowongan = self.client.post(reverse("tambah-lowongan"), data={
												"nama_pekerjaan": "macul",
												"alamat":"jalan",
												"tipe_pekerjaan":"Fulltime",
												"desc":"ini pekerjaan" 
				})

		pk_object = Kantor.objects.get(nama_pekerjaan="macul")
		response_detail_lowongan = self.client.get(reverse('detail-lowongan', kwargs={'pk': pk_object.pk}))
		response_list_detail_lowongan = response = self.client.get(reverse("list-of-applicants", kwargs={'job_id': pk_object.pk}))


		self.assertEqual(response_detail_lowongan.status_code, 200)
		self.assertEqual(response_list_detail_lowongan.status_code, 200)

	#tes tutup lowongan
	def test_tutup_lowongan(self):
		response_register = self.client.post(reverse("register-kantor"),data={
												"username": "jaaa",
												"email": "email3@gmail.com",
												"password1": "admin123",
												"password2": "admin123"
				})

		response_login = self.client.post(reverse("login"), data={
												"username": "jaaa",
												"password": "admin123"
				})

		response_tambah_lowongan = self.client.post(reverse("tambah-lowongan"), data={
												"nama_pekerjaan": "macul",
												"alamat":"jalan",
												"tipe_pekerjaan":"Fulltime",
												"desc":"ini pekerjaan" 
				})

		pk_object = Kantor.objects.get(nama_pekerjaan="macul")
		response_confirmation = self.client.get(reverse("closed-lowongan", kwargs={'pk': pk_object.pk}))
		self.assertEqual(response_confirmation.status_code, 200)

		response_post_confirmation = self.client.post(reverse("closed-lowongan", kwargs={'pk': pk_object.pk}))
		self.assertEqual(response_post_confirmation.status_code, 302)

# Tes fungsi untuk search pekerjaan atau perusahaan
class TestSearchFunction(TestCase):
	def test_search_specific_lowongan(self):
		response = self.client.get('/Kantor/list/pekerjaan')
		self.assertEqual(response.status_code, 200)

	def test_search_all_lowongan(self):
		response = self.client.get('/Kantor/list/all')
		self.assertEqual(response.status_code, 200)

	def test_get_perusahaan(self):
		response = self.client.get('/Kantor/perusahaan/1')
		self.assertEqual(response.status_code, 200)

class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(KantorConfig.name, 'Kantor')
		self.assertEqual(apps.get_app_config('Kantor').name, 'Kantor')
