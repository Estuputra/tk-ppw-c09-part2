from django.contrib import admin
from .models import Kantor, KantorUser

# Register your models here.
admin.site.register(KantorUser)
admin.site.register(Kantor)
