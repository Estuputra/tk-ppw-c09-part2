from django.urls import path
from Kantor.views import *

urlpatterns = [
	path('', landingKantor, name='landing-kantor'), 
	path('edit-profile/', editKantor, name='editProfileKantor'), 

    path('list/', list_lowongan, name='list-lowongan'), #Ini list umum
    path('this-applicants/', list_lowongan_this_kantor, name='this-applicants'), #Ini list kantor ini doang

    path('register/', registerKantor, name='register-kantor'), #register
    path('tambah-lowongan/', daftar_lowongan, name='tambah-lowongan'),
    path('<pk>', detail_pekerjaan, name='detail-lowongan'),
    path('list-of-applicants/P<int:job_id>',
         list_applicants, name='list-of-applicants'),
    path('tutup_lowongan/<pk>', tutup_lowongan, name='closed-lowongan'),

    path('list/<name>', search_lowongan, name='name_lowongan'),
    path('perusahaan/<pk>', get_perusahaan, name='get_perusahaan')
]
