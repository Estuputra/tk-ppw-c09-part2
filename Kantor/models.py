from django.db import models
from django.contrib.auth.models import User

TIPE_PEKERJAAN = (
    ("Fulltime", "Fulltime"),
    ("Part time", "Part time"),
    ("Internship", "Internship")
)

class KantorUser(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    nama = models.CharField(max_length=100, unique=True, null=True)
    alamat = models.CharField(max_length=100, null=True)
    deskripsi_kantor = models.TextField(null=True)

    def __str__(self):
        return self.nama

class Kantor(models.Model):
    Kantor = models.ForeignKey(
        'KantorUser',
        on_delete=models.CASCADE,
    )

    nama_pekerjaan = models.CharField(max_length=100)
    alamat = models.CharField(max_length=100)
    tipe_pekerjaan = models.CharField(choices=TIPE_PEKERJAAN, max_length=15)
    tersedia_sejak = models.DateField(auto_now=True, auto_created=True)
    deskripsi_pekerjaan = models.TextField()

    closed_status = models.BooleanField(default=False)

    def __str__(self):
        return self.nama_pekerjaan
