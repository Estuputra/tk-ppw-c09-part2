from django.apps import AppConfig


class KantorConfig(AppConfig):
    name = 'Kantor'
