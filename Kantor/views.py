from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.models import User, Group
from django.core import serializers
from django.http import HttpResponse, JsonResponse

from login.forms import RegisterForm, LoginForm
from Kantor.forms import FormNamaPerusahaan, FormKantor
from Kantor.models import Kantor, KantorUser
from Jobseeker.models import Jobseeker

TIPE_PEKERJAAN = [
    ("Fulltime"),
    ("Part time"),
    ("Internship")
]


def landingKantor(request):
    if request.user.is_authenticated:
        group_kantor, boolean = Group.objects.get_or_create(name='Kantor')
        if group_kantor in request.user.groups.all():
            return render(request, 'Landing-Kantor-User/landing.html')
        else:
            return redirect('/')

    return render(request, 'Landing-Kantor-User/landing.html')


def editKantor(request):
    context = {}
    context['kantor'] = KantorUser.objects.get(user=request.user)
    form_perusahaan = FormNamaPerusahaan()
    if Group.objects.get(name='Kantor') in request.user.groups.all():
        form_perusahaan = FormNamaPerusahaan()
        if request.method == "POST":
            dict_post = request.POST
            user_to_found = KantorUser.objects.get(
                user=request.user)
            if user_to_found:
                print(dict_post['nama_perusahaan'])
                print(dict_post['alamat'])
                print(dict_post['desc'])
                user_to_found.nama = dict_post['nama_perusahaan']
                user_to_found.alamat = dict_post['alamat']
                user_to_found.deskripsi_kantor = dict_post['desc']
                user_to_found.save()

                return JsonResponse(dict_post)

    context['form'] = form_perusahaan

    return render(request, 'Kantor/login/edit-kantor.html', context)


def registerKantor(request):
    context = {}
    if request.user.is_authenticated:
        return redirect('Kantor/landing-kantor')
    else:
        form = RegisterForm()
        context['form'] = form
        if request.method == "POST":
            form_input = RegisterForm(request.POST)
            if form_input.is_valid():
                username_input = request.POST['username']
                try:
                    User.objects.get(username=username_input)
                    context['user_exist'] = True
                    return render(request, 'Kantor/login/register-kantor.html', context)
                except:
                    pass

                password_input1 = request.POST['password1']
                password_input2 = request.POST['password2']

                if password_input1 != password_input2:
                    context['different_password'] = True
                    return render(request, 'Kantor/login/register-kantor.html', context)

                new_user = User.objects.create_user(
                    username=username_input,
                    email=request.POST['email'],
                    password=password_input1)

                new_user.save()
                KantorUser.objects.create(
                    user=new_user, nama=new_user.username)
                my_group, boolean = Group.objects.get_or_create(name='Kantor')
                my_group.user_set.add(new_user)
                return redirect('/login/login')

        context['form'] = form
        return render(request, 'Kantor/login/register-kantor.html', context)


def list_lowongan_this_kantor(request):
    context = {}
    this_kantor = KantorUser.objects.get(user=request.user)
    all_pekerjaan = Kantor.objects.filter(Kantor=this_kantor)

    context['pekerjaans'] = all_pekerjaan

    return render(request, 'Kantor/list-pekerjaan-kantor-ini.html', context)


def list_lowongan(request):
    context = {}
    all_pekerjaan = Kantor.objects.all()

    context['pekerjaans'] = all_pekerjaan

    group_kantor, boolean = Group.objects.get_or_create(name='Kantor')
    if group_kantor in request.user.groups.all():
        context['is_Kantor'] = True

    return render(request, 'Kantor/list-pekerjaan.html', context)


def detail_pekerjaan(request, pk):
    passed_group1, boolean = Group.objects.get_or_create(name="Jobseeker")
    passed_group2, boolean = Group.objects.get_or_create(name="Kantor")

    # untuk Kantor
    if passed_group2 in request.user.groups.all():
        context = {}
        pekerjaan = Kantor.objects.get(pk=pk)

        context['pekerjaan'] = pekerjaan
        context['is_Kantor'] = True

        return render(request, 'Kantor/detail-pekerjaan.html', context)

    # for Jobseeker
    if passed_group1 in request.user.groups.all():
        context = {}
        pekerjaan = Kantor.objects.get(pk=pk)

        context['pekerjaan'] = pekerjaan
        context['is_JobSeeker'] = True
        if pekerjaan in Jobseeker.objects.get(user=request.user).pekerjaan_dilamar.all():
            context['sudahDilamar'] = "yes"

        return render(request, 'Kantor/detail-pekerjaan.html', context)
    else:
        return render(request, 'gagal.html', {'allowed': 'Jobseeker dan Pembuka Lowongan'})


def tutup_lowongan(request, pk):
    context = {}
    # {% url 'close-lowongan' job_id=pekerjaan.pk %}
    this_kantor = KantorUser.objects.get(user=request.user)
    this_lowongan = Kantor.objects.get(pk=pk)

    if this_lowongan.Kantor == this_kantor:
        if request.method == "POST":
            this_lowongan.closed_status = True
            this_lowongan.save()
            return redirect('detail-lowongan', pk)

        context['pekerjaan'] = this_lowongan
        return render(request, 'Kantor/tutup-lowongan-confirmation.html', context)


def daftar_lowongan(request):
    if request.user.is_authenticated:
        group_kantor, boolean = Group.objects.get_or_create(name='Kantor')
        if group_kantor in request.user.groups.all():
            context = {}
            form = FormKantor()

            if request.method == "POST":
                form_pekerjaan = FormKantor(request.POST)

                if form_pekerjaan.is_valid():
                    data = form_pekerjaan.cleaned_data
                    pekerjaan = Kantor()

                    pekerjaan.Kantor = KantorUser.objects.get(
                        user=request.user)
                    pekerjaan.nama_pekerjaan = data['nama_pekerjaan']
                    pekerjaan.alamat = data['alamat']
                    pekerjaan.tipe_pekerjaan = data['tipe_pekerjaan']
                    pekerjaan.deskripsi_kantor = data['desc']
                    pekerjaan.save()

                    context['form'] = form
                    context['success'] = True
                    context['nama_pekerjaan'] = pekerjaan.nama_pekerjaan

                    return redirect('/Kantor')

            return render(request, 'Kantor/daftarkan-pekerjaan.html', {'form': form})
    return redirect('/Kantor')


def list_applicants(request, job_id):
    context = {}
    pekerjaanDilamar = Kantor.objects.get(pk=job_id)

    context['pekerjaan'] = pekerjaanDilamar
    context['listPelamar'] = Jobseeker.objects.filter(
        pekerjaan_dilamar=job_id)

    return render(request, 'Kantor/list-applicants.html', context)


def search_lowongan(request, name):
    if name == "all":
        search_name = Kantor.objects.all()
    else:
        search_name = Kantor.objects.filter(nama_pekerjaan__contains=name)

    dict_res = serializers.serialize('json', search_name)
    return HttpResponse(dict_res, content_type='application/json')


def get_perusahaan(request, pk):
    search_name = KantorUser.objects.filter(pk=pk)
    dict_res = serializers.serialize('json', search_name)

    return HttpResponse(dict_res, content_type='application/json')
