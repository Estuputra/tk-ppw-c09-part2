from django import forms

class FormNamaPerusahaan(forms.Form):
    nama_perusahaan = forms.CharField(
        label="Nama Perusahaan",
        max_length=64,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
            }
        )
    )

    alamat = forms.CharField(
        label="Alamat",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
            }
        )
    )

    desc = forms.CharField(
        label='Deskripsi',
        widget=forms.Textarea(
            attrs={
                'class': 'form-control'
            }
        )
    )

class FormKantor(forms.Form):
    nama_pekerjaan = forms.CharField(
        label="Nama Pekerjaan",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
            }
        )
    )

    alamat = forms.CharField(
        label="Alamat",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
            }
        )
    )

    tipe_pekerjaan = forms.ChoiceField(
        label='Tipe Pekerjaan',
        widget=forms.RadioSelect(),
        choices=[
            ("Fulltime", "Fulltime"),
            ("Part time", "Part time"),
            ("Internship", "Internship")
        ]
    )

    desc = forms.CharField(
        label='Deskripsi',
        widget=forms.Textarea(
            attrs={
                'class': 'form-control'
            }
        )
    )

