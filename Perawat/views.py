from django.shortcuts import render, redirect
from Perawat.forms import FormPerawat
from RumahSakit.models import RS
from Perawat.models import Perawat
from django.contrib.auth.models import Group
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse

def daftar_perawat(request, id):
    if request.user.is_authenticated:
        if Group.objects.get(name='VolunteerPerawat') in request.user.groups.all():
            exist = False
            perawat_obj = None
            try:
                perawat_obj = Perawat.objects.get(user = request.user)
                if perawat_obj.rumah_sakit.id == id:
                    messages.error(request, f'Anda sudah terdaftar menjadi Volunteer di {perawat_obj.rumah_sakit.nama}')
                    return redirect('/RumahSakit/listRS/')
            except ObjectDoesNotExist:
                pass
            except AttributeError:
                exist = True
            form  = FormPerawat(request.POST)
            rumahsakit = RS.objects.get(id=id)
            if request.method == 'POST':
                if form.is_valid() and request.method == 'POST':
                    data = form.cleaned_data
                    try:
                        perawat = Perawat.objects.get(user=request.user)
                        perawat.nama = data['nama']
                        perawat.umur = data['umur']
                        perawat.email = data['email']
                        perawat.provinsi = data['provinsi']
                        perawat.kota = data['kota']
                        perawat.no_telp = data['no_telp']
                        perawat.pengalaman = data['pengalaman']
                        perawat.alasan = data['alasan']
                        perawat.rumah_sakit = rumahsakit
                        perawat.user = request.user
                        perawat.save()
                    except ObjectDoesNotExist:
                        perawat = Perawat()
                        perawat.nama = data['nama']
                        perawat.umur = data['umur']
                        perawat.email = data['email']
                        perawat.provinsi = data['provinsi']
                        perawat.kota = data['kota']
                        perawat.no_telp = data['no_telp']
                        perawat.pengalaman = data['pengalaman']
                        perawat.alasan = data['alasan']
                        perawat.rumah_sakit = rumahsakit
                        perawat.user = request.user
                        perawat.save()

                    messages.success(request, f'Anda telah berhasil melamar menjadi VolunteerNonPerawat di {rumahsakit.nama}!')
                    
                    return redirect('/RumahSakit/listRS')
                
                messages.error(request, f'Terdapat kesalahan dalam mengisi form!')
            if exist:
                print(perawat_obj.nama)
                context = {
                    'form' : form,
                    'page_title' : 'Daftar Menjadi Relawan Sekarang',
                    'alr' : exist,
                    'obj' : perawat_obj
                }
                return render(request, 'daftar-perawat.html', context)
            else:
                context = {
                    'form' : form,
                    'page_title' : 'Daftar Menjadi Relawan Sekarang',
                    'alr' : exist
                }
                return render(request, 'daftar-perawat.html', context)
        else:
            messages.error(request, 'Akun Anda bukan merupakan akun VolunteerPerawat, sehingga tidak bisa melamar sebagai VolunteerPerawat')
            return redirect('/RumahSakit/listRS')
    return redirect('/login/login/')

def detail_perawat(request, id):
    perawat = Perawat.objects.get(id = id)
    
    return JsonResponse({
        'id' : perawat.id, 
        'nama' : perawat.nama, 
        'umur' : perawat.umur, 
        'provinsi' : perawat.provinsi, 
        'kota' : perawat.kota, 
        'pengalaman' : perawat.pengalaman, 
        'alasan' : perawat.alasan,
        'rumah_sakit_id' : perawat.rumah_sakit.id,
        'approval' : perawat.approval})

def approve_perawat(request, rumah_sakit_id, id):
    perawat = Perawat.objects.get(id = id)
    rs = RS.objects.get(id = rumah_sakit_id)
    rs.jml_prwt -= 1
    perawat.approval = True
    perawat.save()
    rs.save()

    return JsonResponse({'nama' : perawat.nama})

def reject_perawat(request, rumah_sakit_id, id):
    perawat = Perawat.objects.get(id = id)
    rs = RS.objects.get(id = rumah_sakit_id)
    perawat.approval = False
    perawat.rumah_sakit = None
    perawat.save()
    rs.save()

    return JsonResponse({'nama' : perawat.nama})

def finish_perawat(request, rumah_sakit_id, id):
    perawat = Perawat.objects.get(id = id)
    rs = RS.objects.get(id = rumah_sakit_id)
    rs.jml_prwt += 1
    perawat.approval = False
    perawat.rumah_sakit = None
    perawat.save()
    rs.save()

    return JsonResponse({'nama' : perawat.nama})