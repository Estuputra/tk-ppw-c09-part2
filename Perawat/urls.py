from django.urls import path
from Perawat.views import daftar_perawat, detail_perawat, approve_perawat, reject_perawat, finish_perawat

urlpatterns = [
    path('daftar-perawat/<int:id>', daftar_perawat, name='daftarperawat'),
    path('detail/<int:id>', detail_perawat, name='detail_perawat'),
    path('approve/<int:rumah_sakit_id>/<int:id>', approve_perawat, name='approve_perawat'),
    path('reject/<int:rumah_sakit_id>/<int:id>', reject_perawat, name='reject_perawat'),
    path('finish/<int:rumah_sakit_id>/<int:id>', finish_perawat, name='finish_perawat'),
]
