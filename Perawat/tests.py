from django.test import TestCase, Client
from django.apps import apps
from django.urls import reverse
from django.contrib.auth.models import User, Group
from Perawat.models import Perawat
from RumahSakit.models import RS
from Perawat.apps import PerawatConfig

class TestPerawat(TestCase):
    def setUp(self):
        Group.objects.create(name='RumahSakit')
        rs_user = User.objects.create_user(username='testuser', password='12345')
        rs = RS.objects.create(
            nama='aaaa',
            alamat = 'Jonggol',
            jml_prwt = '2',
            jml_non = '2',
            user = rs_user
        )
        rs.save()
        Group.objects.create(name='VolunteerPerawat')
        self.user = User.objects.create_user(username='testuser2', password='12345')
        Group.objects.get(name='VolunteerPerawat').user_set.add(self.user)
        login = self.client.login(username='testuser2', password='12345')

    def test_url(self):
        response = self.client.get(reverse('daftarperawat', args=[1]))
        self.assertEquals(response.status_code, 200)

    def test_template(self):
        response = self.client.get(reverse('daftarperawat', args=[1]))
        self.assertTemplateUsed(response, 'daftar-perawat.html')

    def test_obj(self):
        # rs = RS.objects.create(nama ='aaaa', alamat='Jonggol', jml_prwt = '2', jml_non = '2')
        ex = Perawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia', user=self.user)
        self.assertEquals(Perawat.objects.all().count(), 1)

    def test_form(self):
        # rs = RS.objects.create(nama ='aaaa', alamat='Jonggol', jml_prwt = '2', jml_non = '2')
        res = self.client.post(reverse('daftarperawat', args=[1]), data={'nama' : 'Budi', 'umur' : '19', 
            'email' : 'budibudiman@gmail.com', 'provinsi' : '13', 'kota' : 'Jakarta Timur', 'no_telp' : '081219593496',
            'pengalaman' : 'Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', 'alasan' : 'Ingin membantu sesama manusia', 'user' :self.user})

        self.assertEquals(res.status_code, 302)
        self.assertEquals(Perawat.objects.all().count(), 1)

    def test_daftar_perawat_di_rumah_sakit_yang_sama(self):
        rs = RS.objects.get(id= 1)
        perawat = Perawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia', user=self.user, rumah_sakit = rs)
        response = self.client.post(reverse('daftarperawat', args=[1]), follow=True)

        self.assertRedirects(response, '/RumahSakit/listRS/')

    def test_daftar_perawat_di_rumah_sakit_null(self):
        rs = RS.objects.get(id= 1)
        perawat = Perawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia', user=self.user, rumah_sakit = None)
        response = self.client.post(reverse('daftarperawat', args=[1]), follow=True)
        self.assertEquals(response.context['alr'], True)

    def test_daftar_perawat_yang_belum_ada_rs(self):
        perawat = Perawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia', user=self.user, rumah_sakit = None)
        response = self.client.post(reverse('daftarperawat', args=[1]), {'nama' : 'Budi', 'umur' : '19', 
            'email' : 'budibudiman@gmail.com', 'provinsi' : '13', 'kota' : 'Jakarta Timur', 'no_telp' : '+6281219593496',
            'pengalaman' : 'Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', 'alasan' : 'Ingin membantu sesama manusia', 'user' : self.user})
        self.assertEquals(response.status_code, 200)

    def test_daftar_perawat_tapi_bukan_groupnya(self):
        self.user = User.objects.create_user(username='testuser3', password='12345')
        login = self.client.login(username='testuser3', password='12345') 
        response = self.client.post(reverse('daftarperawat', args=[1]), {'nama' : 'Budi', 'umur' : '19', 
            'email' : 'budibudiman@gmail.com', 'provinsi' : '13', 'kota' : 'Jakarta Timur', 'no_telp' : '+6281219593496',
            'pengalaman' : 'Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', 'alasan' : 'Ingin membantu sesama manusia', 'user' : self.user}, follow=True)
        
        self.assertRedirects(response, '/RumahSakit/listRS/')

    def test_detail_perawat(self):
        rs = RS.objects.get(id= 1)
        perawat = Perawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia', user=self.user, rumah_sakit = rs)
        response = Client().get(reverse('detail_perawat', args=[1]))
        self.assertJSONEqual(response.content, {
            "id": 1, 
            "nama": "Budi", 
            "umur": 19, 
            "provinsi": "13", 
            "kota": "Jakarta Timur", 
            "pengalaman": "Menjadi relawan kemanusiaan saat terjadi gempa di Lombok", 
            "alasan": "Ingin membantu sesama manusia", 
            "rumah_sakit_id": 1, 
            "approval": None})
    
    def test_approve_perawat(self):
        rs = RS.objects.get(id= 1)
        perawat = Perawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia', user=self.user, rumah_sakit = rs)
        response = Client().get(reverse('approve_perawat', args=[1,1]))
        self.assertJSONEqual(response.content, {"nama": "Budi"})

    def test_reject_perawat(self):
        rs = RS.objects.get(id= 1)
        perawat = Perawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia', user=self.user, rumah_sakit = rs)
        response = Client().get(reverse('reject_perawat', args=[1,1]))
        self.assertJSONEqual(response.content, {"nama": "Budi"})

    def test_finish_perawat(self):
        rs = RS.objects.get(id= 1)
        perawat = Perawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia', user=self.user, rumah_sakit = rs)
        response = Client().get(reverse('finish_perawat', args=[1,1]))
        self.assertJSONEqual(response.content, {"nama": "Budi"})

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(PerawatConfig.name, 'Perawat')
        self.assertEqual(apps.get_app_config('Perawat').name, 'Perawat')
