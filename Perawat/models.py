from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from RumahSakit.models import RS
from django.contrib.auth.models import User 

class Perawat(models.Model):
    PROVINSI = (
        ('1', 'Aceh'),
        ('2', 'Sumatra Utara'),
        ('3', 'Sumatra Barat'),
        ('4', 'Riau'),
        ('5', 'Kepulauan Riau'),
        ('6', 'Bengkulu'),
        ('7', 'Jambi'),
        ('8', 'Sumatra Selatan'),
        ('9', 'Bangka Belitung'),
        ('10', 'Lampung'),
        ('11', 'Banten'),
        ('12', 'Jawa Barat'),
        ('13', 'DKI Jakarta'),
        ('14', 'Jawa Tengah'),
        ('15', 'Yogyakarta'),
        ('16', 'Jawa Timur'),
        ('17', 'Bali'),
        ('18', 'NTB'),
        ('19', 'NTT'),
        ('20', 'Kalimantan Timur'),
        ('21', 'Kalimantan Barat'),
        ('22', 'Kalimantan Selatan'),
        ('23', 'Kalimantan Tengah'),
        ('24', 'Kalimantan Utara'),
        ('25', 'Gorontalo'),
        ('26', 'Sulawesi Barat'),
        ('27', 'Sulawesi Tengah'),
        ('28', 'Sulawesi Selatan'),
        ('29', 'Sulawesi Tenggara'),
        ('30', 'Sulawesi Utara'),
        ('31', 'Maluku'),
        ('32', 'Maluku Utara'),
        ('33', 'Papua Barat'),
        ('34', 'Papua'),
    )
    nama = models.CharField(max_length=100, null=True)
    umur = models.IntegerField(
        validators=[
            MaxValueValidator(55),
            MinValueValidator(17)
        ], null=True)
    email = models.EmailField(null=True)
    provinsi = models.CharField(max_length=2, choices=PROVINSI, null=True)
    kota = models.CharField(max_length=50, null=True)
    no_telp = models.IntegerField(
        validators=[
            MaxValueValidator(999999999999),
            MinValueValidator(99999)
        ], null=True)
    pengalaman = models.TextField(null=True)
    alasan = models.TextField(null=True)
    rumah_sakit = models.ForeignKey(RS, on_delete=models.CASCADE, null = True, blank = True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null = True)
    approval = models.BooleanField(null=True)