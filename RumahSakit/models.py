from django.db import models
from django.conf import settings
from django.core.validators import MinValueValidator
from django.contrib.auth.models import User 

# Create your models here.
class RS(models.Model):
    nama = models.TextField(max_length=100, unique=True)
    alamat = models.TextField(max_length=150, unique=True)
    jml_prwt = models.PositiveIntegerField()
    jml_non = models.PositiveIntegerField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank = True) 

    def __str__(self):
        return self.nama