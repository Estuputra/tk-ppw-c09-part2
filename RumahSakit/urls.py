from django.urls import path
from . import views

app_name = 'RumahSakit'

urlpatterns = [
    path('daftarRS/', views.daftarRS, name='daftarRS'),
    path('editRS/', views.editRS, name='editRS'),
    path('editProses/', views.editProses, name='editProses'),
    path('listRS/', views.listRS, name = 'listRS'),
    path('<int:id>/', views.detailRS, name='detailRS'),
    path('ListPendaftar/<int:id>', views.list_pendaftar, name='list-pendaftar'),
    path('ListPendaftarVNP/<int:id>', views.list_pendaftar_ajax, name='list-pendaftar-ajax'),
    path('ListPendaftarPerawat/<int:id>', views.list_pendaftar_ajax_2, name='list-pendaftar-ajax-2'),
]