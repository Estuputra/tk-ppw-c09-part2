from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .models import RS
from Perawat.models import Perawat
from VolunteerNonPerawat.models import VolunteerNonPerawat
from .forms import RSForm
from itertools import zip_longest
from django.contrib.auth import authenticate
from django.contrib.auth.models import Group
import json
from django.contrib.auth.decorators import login_required, user_passes_test
from .decorators import cekRS, noRS, adaRS
from django.contrib import messages
from django.http import HttpResponse
from django.core import serializers

# Create your views here.
@login_required(login_url='/login/login/')
@user_passes_test(cekRS, login_url='/login/login/')
@user_passes_test(noRS, login_url='/RumahSakit/listRS')
def daftarRS(request):
    if not Group.objects.get(name='RumahSakit') in request.user.groups.all():
        messages.error(request, "Akun Anda bukan merupakan akun RumahSakit, sehingga tidak bisa mendaftarkan Rumah Sakit")
        return redirect('/')

    if request.method == "POST":
        RS_form = RSForm(request.POST)
        if RS_form.is_valid():
            RS_form.instance.user = request.user
            RS_form.save()
        else:
            return redirect("RumahSakit:daftarRS")
        return redirect("RumahSakit:listRS")
    
    context = {
        "RSform" : RSForm
    }

    return render(request, 'daftar.html', context)

@login_required(login_url='/login/login/')
@user_passes_test(cekRS, login_url='/login/login/')
@user_passes_test(adaRS, login_url='/RumahSakit/daftarRS')
def editRS(request):
    context = {}
    context['rs'] = RS.objects.get(user=request.user)
    RS_form = RSForm(request.POST)
    context['RSform'] = RS_form
    return render(request, 'editRS.html', context)

def editProses(request):
    if request.method == "POST":
        dict_post = request.POST
        rs = RS.objects.get(user=request.user)
        if rs:
            rs.nama = dict_post['nama']
            rs.alamat = dict_post['alamat']
            rs.jml_prwt = dict_post['jml_prwt']
            rs.jml_non = dict_post['jml_non']
            rs.save()
            return JsonResponse({})

def listRS(request):
    context = {}
    all_RS = RS.objects.all()

    context['all_RS'] = all_RS

    return render(request, 'list-RS.html', context)

def detailRS(request, id):
    context = {}
    RSakit = RS.objects.get(id=id)

    context['RS'] = RSakit
    context['right_user'] = RSakit.user == request.user

    return render(request, 'detail-RS.html', context)


@login_required(login_url='/RumahSakit/listRS')
@user_passes_test(cekRS, login_url='/RumahSakit/listRS')
def list_pendaftar(request, id):
    if not Group.objects.get(name='RumahSakit') in request.user.groups.all():
        messages.error(request, "Akun Anda bukan merupakan akun RumahSakit, sehingga tidak bisa mendaftarkan Rumah Sakit")
        return redirect('/')
    context = {}
    rumahsakit = RS.objects.get(id = id)
    if rumahsakit.user == request.user:
        context['RS'] = rumahsakit
        listPerawat = Perawat.objects.filter(rumah_sakit_id = id)
        listNonPerawat = VolunteerNonPerawat.objects.filter(rumah_sakit_id = id)
        context['listVolunteer'] = zip_longest(listPerawat, listNonPerawat)

        return render(request, 'list_pendaftar.html', context)
    return redirect("RumahSakit:listRS")

def list_pendaftar_ajax(request, id):
    rumahsakit = RS.objects.get(id = id)
    if rumahsakit.user == request.user:
        listNonPerawat = VolunteerNonPerawat.objects.filter(rumah_sakit_id = id)
        dict_res = serializers.serialize('json', listNonPerawat)
        return HttpResponse(dict_res, content_type='application/json')
    return HttpResponse()

def list_pendaftar_ajax_2(request, id):
    rumahsakit = RS.objects.get(id = id)
    if rumahsakit.user == request.user:
        listPerawat = Perawat.objects.filter(rumah_sakit_id = id)
        dict_res = serializers.serialize('json', listPerawat)
        return HttpResponse(dict_res, content_type='application/json')
    return HttpResponse()