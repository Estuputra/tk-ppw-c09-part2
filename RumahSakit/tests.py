from django.test import TestCase, Client
from RumahSakit.models import RS
from RumahSakit.forms import RSForm
from django.urls import reverse
from RumahSakit.apps import RumahsakitConfig
from django.apps import apps
from Perawat.models import Perawat
from VolunteerNonPerawat.models import VolunteerNonPerawat
from django.contrib.auth.models import User, Group

# Create your tests here.
class ModelTest(TestCase):
    def setUp(self):
        Group.objects.create(name='RumahSakit')
        self.user = User.objects.create_user(username='testuser', password='12345')
        Group.objects.get(name='RumahSakit').user_set.add(self.user)
        login = self.client.login(username='testuser', password='12345')

        self.RS_test = RS.objects.create(
            nama = "RSJ Test",
            alamat = "Grogol",
            jml_prwt = 1,
            jml_non = 1,
            user = self.user
        )

    def test_instance_created(self):
        self.assertEqual(RS.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.RS_test), "RSJ Test")
    
    def test_nama(self):
        self.assertEqual(self.RS_test.nama, "RSJ Test")

    def test_alamat(self):
        self.assertEqual(self.RS_test.alamat, "Grogol")
    
    def test_jmlprwt(self):
        self.assertEqual(self.RS_test.jml_prwt, 1)

    def test_jmlnon(self):
        self.assertEqual(self.RS_test.jml_non, 1)


class FormTest(TestCase):
    def setUp(self):
        Group.objects.create(name='RumahSakit')
        self.user = User.objects.create_user(username='testuser', password='12345')
        Group.objects.get(name='RumahSakit').user_set.add(self.user)
        login = self.client.login(username='testuser', password='12345')

    def test_form_is_valid(self):
        form_RS = RSForm(
            data={
                'nama': "RSJ Test",
                'alamat': "Grogol",
                'jml_prwt': 1,
                "jml_non": 1,
                "user": self.user
            }
        )
        self.assertTrue(form_RS.is_valid())

    def test_form_invalid(self):
        form_RS = RSForm(
            data={
            }
        )
        self.assertFalse(form_RS.is_valid())

class UrlTest(TestCase):
    def setUp(self):
        Group.objects.create(name='RumahSakit')
        self.user = User.objects.create_user(username='testuser', password='12345')
        Group.objects.get(name='RumahSakit').user_set.add(self.user)
        self.client = Client()
        self.client.force_login(self.user)

    def test_url(self):
        RS_test = RS.objects.create(
            nama = "RSJ Test",
            alamat = "Grogol",
            jml_prwt = 1,
            jml_non = 1,
            user = self.user
        )
        RS_test.save()
        

class ViewTest(TestCase):
    def setUp(self):
        Group.objects.create(name='RumahSakit')
        self.user = User.objects.create_user(username='testuser', password='12345')
        Group.objects.get(name='RumahSakit').user_set.add(self.user)
        login = self.client.login(username='testuser', password='12345')

    def test_daftar_GET(self):
        response = self.client.get(reverse("RumahSakit:daftarRS"))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "daftar.html")

    def test_add_accepted_RS_POST(self):
        response = self.client.post(reverse("RumahSakit:daftarRS"),
                {"nama" : "RSJ Test",
                'alamat': "Grogol",
                'jml_prwt': 1,
                "jml_non": 1,})
        self.assertEquals(response.status_code, 302)
    
    def test_add_restricted_RS_POST(self):
        response = self.client.post(reverse("RumahSakit:daftarRS"),
                {"nama" : "RSJ Test",
                'alamat': "Grogol",
                'jml_prwt': 1,
                "jml_non": 1,
                "user": self.user})
        self.assertEquals(response.status_code, 302)
    
    def test_get_detailRS(self):
        rms = RS.objects.create(
                nama = "RSJ Test",
                alamat = "Grogol",
                jml_prwt = 1,
                jml_non = 1,
                user = self.user
            )

        response = Client().get(reverse("RumahSakit:detailRS", kwargs={'id': rms.id}))
        self.assertEqual(response.status_code, 200)

    def test_get_listRS(self):
        response = Client().get(reverse("RumahSakit:listRS"))
        self.assertEqual(response.status_code, 200)

    def test_get_detailRS_restricted(self):
        userbeda = User.objects.create_user(username='userbeda', password='12345')
        rms = RS.objects.create(
                nama = "RSJ Test",
                alamat = "Grogol",
                jml_prwt = 1,
                jml_non = 1,
                user = userbeda
            )
        response = self.client.get(reverse("RumahSakit:list-pendaftar", kwargs={'id': rms.id}))
        self.assertEqual(response.status_code, 302)


    def test_get_detailRS_nolPelamar(self):
        rms = RS.objects.create(
                nama = "RSJ Test",
                alamat = "Grogol",
                jml_prwt = 1,
                jml_non = 1,
                user = self.user
            )
        jml_prwt = Perawat.objects.filter(rumah_sakit_id = rms.id)
        jml_non = VolunteerNonPerawat.objects.filter(rumah_sakit_id = rms.id)
        response = self.client.get(reverse("RumahSakit:list-pendaftar", kwargs={'id': rms.id}))
        
        self.assertEqual(len(jml_prwt), 0)
        self.assertEqual(len(jml_non), 0)
        self.assertEqual(response.status_code, 200)

    def test_get_detailRS_satuPerawat(self):
        rms = RS.objects.create(
                nama = "RSJ Test",
                alamat = "Grogol",
                jml_prwt = 1,
                jml_non = 1,
                user = self.user
            )

        prwt = Perawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia'
            ,rumah_sakit=rms)

        jml_prwt = Perawat.objects.filter(rumah_sakit_id = rms.id)
        jml_non = VolunteerNonPerawat.objects.filter(rumah_sakit_id = rms.id)
        response = self.client.get(reverse("RumahSakit:list-pendaftar", kwargs={'id': rms.id}))
        
        self.assertEqual(len(jml_prwt), 1)
        self.assertEqual(len(jml_non), 0)
        self.assertEqual(response.status_code, 200)

    def test_get_detailRS_satuNonPerawat(self):
        rms = RS.objects.create(
                nama = "RSJ Test",
                alamat = "Grogol",
                jml_prwt = 1,
                jml_non = 1,
                user = self.user
            )

        non = VolunteerNonPerawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia'
            ,rumah_sakit=rms)

        jml_prwt = Perawat.objects.filter(rumah_sakit_id = rms.id)
        jml_non = VolunteerNonPerawat.objects.filter(rumah_sakit_id = rms.id)
        response = self.client.get(reverse("RumahSakit:list-pendaftar", kwargs={'id': rms.id}))
        
        self.assertEqual(len(jml_prwt), 0)
        self.assertEqual(len(jml_non), 1)
        self.assertEqual(response.status_code, 200)

    def test_get_detailRS_satuForAll(self):
        rms = RS.objects.create(
                nama = "RSJ Test",
                alamat = "Grogol",
                jml_prwt = 1,
                jml_non = 1,
                user = self.user
            )

        prwt = Perawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia'
            ,rumah_sakit=rms)

        non = VolunteerNonPerawat.objects.create(nama='Anduk', umur=69, 
            email='Andukin@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593469',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Kasur', alasan='Ingin membantu sesama hewan'
            ,rumah_sakit=rms)

        jml_prwt = Perawat.objects.filter(rumah_sakit_id = rms.id)
        jml_non = VolunteerNonPerawat.objects.filter(rumah_sakit_id = rms.id)
        response = self.client.get(reverse("RumahSakit:list-pendaftar", kwargs={'id': rms.id}))
        
        self.assertEqual(len(jml_prwt), 1)
        self.assertEqual(len(jml_non), 1)
        self.assertEqual(response.status_code, 200)
    
    def test_list_pendaftar_ajax(self):
        rms = RS.objects.create(
                nama = "RSJ Test",
                alamat = "Grogol",
                jml_prwt = 1,
                jml_non = 1,
                user = self.user
            )

        # prwt = Perawat.objects.create(nama='Budi', umur=19, 
        #     email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593496',
        #     pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia'
        #     ,rumah_sakit=rms)

        non = VolunteerNonPerawat.objects.create(nama='Anduk', umur=69, 
            email='Andukin@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593469',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Kasur', alasan='Ingin membantu sesama hewan'
            ,rumah_sakit=rms)

        # jml_prwt = Perawat.objects.filter(rumah_sakit_id = rms.id)
        jml_non = VolunteerNonPerawat.objects.filter(rumah_sakit_id = rms.id)
        response = self.client.get(reverse("RumahSakit:list-pendaftar-ajax", kwargs={'id': rms.id}))
        
        # self.assertEqual(len(jml_prwt), 1)
        self.assertEqual(len(jml_non), 1)
        self.assertEqual(response.status_code, 200)
    
    def test_redirect_user_if_not_rumahsakit(self):
        self.user = User.objects.create_user(username='testuserr', password='12345')
        login = self.client.login(username='testuserr', password='12345')
        response = self.client.get(reverse('RumahSakit:daftarRS'), follow=True)

        self.assertRedirects(response, '/')
    
    def test_list_pendaftar_ajax_user_not_rs(self):
        new_user = User.objects.create_user(username='testuserrr', password='12345')
        Group.objects.get(name='RumahSakit').user_set.add(new_user)
        
        RS_test = RS.objects.create(
            nama = "RSJ Test",
            alamat = "Grogol",
            jml_prwt = 1,
            jml_non = 1,
            user = new_user
        )
        RS_test.save()

        response = self.client.get('/RumahSakit/ListPendaftarVNP/1')
        self.assertEquals(response.content, b'')

    def test_edit_accepted_RS_POST(self):
        response = self.client.post(reverse("RumahSakit:daftarRS"),
                {"nama" : "RSJ Test",
                'alamat': "Grogol",
                'jml_prwt': 1,
                "jml_non": 1,})

        response = self.client.post(reverse("RumahSakit:editRS"),
                {"nama" : "RSJ Gotham",
                'alamat': "Gotham",
                'jml_prwt': 2,
                "jml_non": 2,})
        self.assertEquals(response.status_code, 200)

    def test_edit_denied_RS_POST(self):
        self.client.logout()
        Group.objects.create(name='JobSeeker')
        self.user = User.objects.create_user(username='testuser2', password='12345')
        Group.objects.get(name='JobSeeker').user_set.add(self.user)
        login = self.client.login(username='testuser2', password='12345')

        response = self.client.post(reverse("RumahSakit:editRS"),
                {"nama" : "RSJ Gotham",
                'alamat': "Gotham",
                'jml_prwt': 2,
                "jml_non": 2,})
        self.assertEquals(response.status_code, 302)

    def test_edit_proses_RS_POST(self):
        response = self.client.post(reverse("RumahSakit:daftarRS"),
                {"nama" : "RSJ Test",
                'alamat': "Grogol",
                'jml_prwt': 1,
                "jml_non": 1,})

        response = self.client.post(reverse("RumahSakit:editProses"),
                {"nama" : "RSJ Gotham",
                'alamat': "Gotham",
                'jml_prwt': 2,
                "jml_non": 2,})
        self.assertEquals(response.status_code, 200)
    
    # def test_list_pendaftar_user_not_rs(self):
    #     new_user = User.objects.create_user(username='testuserrr', password='12345')
    #     Group.objects.get(name='RumahSakit').user_set.add(new_user)
        
    #     RS_test = RS.objects.create(
    #         nama = "RSJ Test",
    #         alamat = "Grogol",
    #         jml_prwt = 1,
    #         jml_non = 1,
    #         user = new_user
    #     )
    #     RS_test.save()

    #     response = self.client.get('/RumahSakit/ListPendaftar/1', follow=True)
    #     self.assertRedirects(response, '/')

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(RumahsakitConfig.name, 'RumahSakit')
        self.assertEqual(apps.get_app_config('RumahSakit').name, 'RumahSakit')