from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth.models import Group
from django.contrib.auth.decorators import login_required, user_passes_test
from .models import RS

def cekRS(user):
    try:
        if user.groups.all()[0].name == "RumahSakit":
            return True
        else:
            return False
    except:
        return False

def noRS(user):
    return RS.objects.filter(user = user).count() == 0

def adaRS(user):
    return RS.objects.filter(user = user).count() == 1
