$(document).ready(function () {
    $('.closebtn').on('click', function () {
        $('.alert').css('opacity', '0');
    });

    $('#btnEdit').on('click', function (e) {
        $('.alert').css('opacity', '0');
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: "/RumahSakit/editProses/",
            data: {
                nama: $('#id_nama').val(),
                alamat: $('#id_alamat').val(),
                jml_prwt: $('#prwt').val(),
                jml_non: $('#non').val(),
                csrfmiddlewaretoken: $('input[name = csrfmiddlewaretoken]').val(),
            },
            success: function (response) {
                $('.alert').css('opacity', '1');
                $('#id_nama').val('');
                $('#id_alamat').val('');
                $('#prwt').val('');
                $('#non').val('');
            },
        });
    });
});