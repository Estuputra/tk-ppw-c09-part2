from django.shortcuts import render, redirect
from RumahSakit.models import RS
from django.contrib.auth.models import Group

# Create your views here.


def home(request):
	try:
		grup_kantor = Group.objects.get(name='Kantor')
		if grup_kantor in request.user.groups.all():
			return redirect('/Kantor')
	except:
		pass
	
	return render(request, "landing.html")
