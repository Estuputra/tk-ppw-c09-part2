from django.test import TestCase, Client
from django.apps import apps
from .apps import TK1_PPWConfig

# class Test(TestCase):
    # def test_00_urls(self):
    #     response = Client().get('/DaftarRelawan/')
    #     self.assertEqual(response.status_code, 200)
    # def test_01_template_used(self):
    #     response = Client().get('/DaftarRelawan/')
    #     self.assertTemplateUsed(response, 'daftar_relawan.html')

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(TK1_PPWConfig.name, 'TK1_PPW')
        self.assertEqual(apps.get_app_config('TK1_PPW').name, 'TK1_PPW')