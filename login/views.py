from django.shortcuts import render, redirect, HttpResponse
from django.http import JsonResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import Group
from .forms import RegisterForm, LoginForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import messages

from Jobseeker.forms import FormPelamar
from Jobseeker.models import Jobseeker

# Create your views here.


def register(request):
    # print(request.user.is_authenticated)
    if request.user.is_authenticated:
        return redirect('/')
    else:
        form = RegisterForm()
        if request.method == "POST":
            form_input = RegisterForm(request.POST)
            if form_input.is_valid():
                username_input = request.POST['username']
                password_input = request.POST['password1']
                try:
                    User.objects.get(username=username_input)
                    messages.error(request, 'Username Anda sudah terdaftar')
                    return redirect('/')
                except:
                    pass
                new_user = User.objects.create_user(
                    username=username_input, email=request.POST['email'], password=password_input)
                new_user.save()
                user_login = authenticate(
                    request, username=username_input, password=password_input)
                login(request, user_login)
                if(request.POST['typePekerjaan'] == 'Jobseeker'):
                    my_group, created = Group.objects.get_or_create(name='Jobseeker')
                    my_group.user_set.add(new_user)
                    pelamarBaru = Jobseeker()
                    pelamarBaru.user = User.objects.get(pk=request.user.pk)
                    pelamarBaru.nama_pelamar = request.user.username
                    pelamarBaru.save()
                    messages.success(request, f'Selamat datang, {username_input}!')
                    return redirect('/login/editProfile')
                elif request.POST['typePekerjaan'] == 'RumahSakit':
                    my_group, created = Group.objects.get_or_create(name='RumahSakit')
                    my_group.user_set.add(new_user)
                    messages.success(request, f'Selamat datang, {username_input}!')
                    return redirect('/RumahSakit/daftarRS')
                elif request.POST['typePekerjaan'] == 'VolNonPerawat':
                    my_group, created = Group.objects.get_or_create(name='VolunteerNonPerawat')
                    my_group.user_set.add(new_user)
                    messages.success(request, f'Selamat datang, {username_input}!')
                    return redirect('/RumahSakit/listRS')
                elif request.POST['typePekerjaan'] == 'VolPerawat' :
                    my_group, created = Group.objects.get_or_create(name='VolunteerPerawat')
                    my_group.user_set.add(new_user)
                    messages.success(request, f'Selamat datang, {username_input}!')
                    return redirect('/RumahSakit/listRS')
                return redirect('/')
            else:
                return render(request, 'login/register.html', {'form': form_input})
        return render(request, 'login/register.html', {'form': form})


choices = [
    ('1', 'Aceh'),
    ('2', 'Sumatera Utara'),
    ('3', 'Sumatera Barat'),
    ('4', 'Riau'),
    ('5', 'Kepulauan Riau'),
    ('6', 'Bengkulu'),
    ('7', 'Jambi'),
    ('8', 'Sumatera Selatan'),
    ('9', 'Bangka Belitung'),
    ('10', 'Lampung'),
    ('11', 'Banten'),
    ('12', 'Jawa Barat'),
    ('13', 'Dki Jakarta'),
    ('14', 'Jawa Tengah'),
    ('15', 'Di Yogyakarta'),
    ('16', 'Jawa Timur'),
    ('17', 'Bali'),
    ('18', 'Nusa Tenggara Barat'),
    ('19', 'Nusa Tenggara Timur'),
    ('20', 'Kalimantan Timur'),
    ('21', 'Kalimantan Barat'),
    ('22', 'Kalimantan Selatan'),
    ('23', 'Kalimantan Tengah'),
    ('24', 'Kalimantan Utara'),
    ('25', 'Gorontalo'),
    ('26', 'Sulawesi Barat'),
    ('27', 'Sulawesi Tengah'),
    ('28', 'Sulawesi Selatan'),
    ('29', 'Sulawesi Tenggara'),
    ('30', 'Sulawesi Utara'),
    ('31', 'Maluku'),
    ('32', 'Maluku Utara'),
    ('33', 'Papua Barat'),
    ('34', 'Papua'),
]
CHOICES_CLEANED = []
for i in choices:
    CHOICES_CLEANED.append(i[1])


def editProfile(request):
    print(request.user.groups.all())
    if Group.objects.get(name='Jobseeker') in request.user.groups.all():
        form_pelamar = FormPelamar()
        if request.method == "POST":
            dict_post = request.POST
            user_to_found = Jobseeker.objects.filter(
                nama_pelamar=dict_post['nama_pelamar'])
            if len(user_to_found) == 0:
                pelamarBaru = Jobseeker()
                pelamarBaru.user = User.objects.get(pk=request.user.pk)
                pelamarBaru.nama_pelamar = dict_post['nama_pelamar']
                pelamarBaru.umur_pelamar = dict_post['umur_pelamar']
                pelamarBaru.provinsi_pelamar = dict_post['provinsi_pelamar']
                pelamarBaru.kota_pelamar = dict_post['kota_pelamar']
                pelamarBaru.pendidikan_terakhir = dict_post['pendidikan_pelamar']
                pelamarBaru.save()
            else:
                pelamar_lama = user_to_found[0]
                pelamar_lama.umur_pelamar = dict_post['umur_pelamar']
                pelamar_lama.provinsi_pelamar = dict_post['provinsi_pelamar']
                pelamar_lama.kota_pelamar = dict_post['kota_pelamar']
                pelamar_lama.pendidikan_terakhir = dict_post['pendidikan_pelamar']
                pelamar_lama.save()
            dict_resp = {}
            for k in dict_post:
                dict_resp[k] = dict_post[k]

            dict_resp['value_prov'] = CHOICES_CLEANED[int(
                dict_post['provinsi_pelamar']) - 1]
            return JsonResponse(dict_resp)
        else:
            user_to_found = Jobseeker.objects.filter(
                nama_pelamar=request.user.username)
            if user_to_found[0].provinsi_pelamar == "":
                # gaketemu
                return render(request, 'Jobseeker/editJobseeker.html', {'form': form_pelamar, 'listProvinsi': CHOICES_CLEANED, 'status': 'tidak_ditemukan'})
            else:
                return render(request, 'Jobseeker/editJobseeker.html', {'form': FormPelamar(initial={'umur_pelamar': user_to_found[0].umur_pelamar, 'kota_pelamar': user_to_found[0].kota_pelamar, 'pendidikan_terakhir': user_to_found[0].pendidikan_terakhir}), 'listProvinsi': CHOICES_CLEANED, 'status': 'ditemukan', 'nomor_prov': int(user_to_found[0].provinsi_pelamar)})


def loginCustom(request):
    try:
        next_page = request.GET['next']
    except:
        next_page = None
        pass
    if request.user.is_authenticated:
        return redirect('/')
    form = LoginForm()
    if request.method == "POST":
        data = LoginForm(request.POST)
        if data.is_valid():
            username_input = request.POST['username']
            password_input = request.POST['password']
            user_login = authenticate(
                request, username=username_input, password=password_input)
            print(user_login)
            if user_login is None:
                return render(request, 'login/login.html', {'status': 'failed', 'form': form})
            else:
                login(request, user_login)
                if next_page is not None and next_page != '':
                    return HttpResponseRedirect(next_page)
                messages.success(request, f'Selamat datang kembali, {username_input}!')
                return redirect('/')
        else:
            return render(request, 'login/login.html', {'status': 'failed', 'form': data})
    else:
        return render(request, 'login/login.html', {'form': form})


def index(request):
    print("hi")
    return render(request, 'login/index.html')


def logoutCustom(request):
    logout(request)
    return redirect('/')


def rLogout(request):
    logout(request)
    return redirect('/login/register')
