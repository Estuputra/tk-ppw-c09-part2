from .apps import LoginConfig
from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from django.contrib.auth.models import User
from .forms import LoginForm, RegisterForm
from django.contrib.auth.models import Group
from Jobseeker.models import Jobseeker
from RumahSakit.models import RS
from VolunteerNonPerawat.models import VolunteerNonPerawat


# Create your tests here.
class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(LoginConfig.name, 'login')
        self.assertEqual(apps.get_app_config('login').name, 'login')


class Story9UnitTest(TestCase):
    def test_homepage_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_signup_url_exists(self):
        response = Client().get('/login/register/')
        self.assertEqual(response.status_code, 200)

    def test_logout_url_exist(self):
        response = Client().get('/login/logout/')
        self.assertEqual(response.status_code, 302)

    def test_login_url_exist(self):
        response = Client().get('/login/login/')
        self.assertEqual(response.status_code, 200)

    def test_forms_login_valid(self):
        form_login = LoginForm(data={
            "username": "hahaha",
            "password": "hihihihi123"
        })
        self.assertTrue(form_login.is_valid())

    def test_forms_login_invalid(self):
        form_login = LoginForm(data={
            "username": "a",
            "password": ""
        })
        self.assertFalse(form_login.is_valid())

    def test_forms_register_valid(self):
        form_reg = RegisterForm(data={
            "username": "hahaha",
            "email": "haha@gmail.com",
            'password1': '123321hahaha',
            'password2': '123321hahaha',
        })
        self.assertTrue(form_reg.is_valid())

    def test_forms_register_valid(self):
        form1 = RegisterForm(data={
            "username": "",
            "email": "haha@gmail.com",
            'password1': '123321hahaha',
            'password2': '123321a',
        })
        self.assertFalse(form1.is_valid())


class ViewsTest(TestCase):
    def setUp(self):
        self.user_login = User.objects.create_user(
            username='testes', password='123')
        self.user_js = User.objects.create_user(
            username='pencarikerja', password='123')
        self.client = Client()
        self.login = reverse("login")
        self.reg = reverse("register")
        self.edit = reverse('editProfile')
        self.group_js = Group.objects.create(name="Jobseeker")
        self.group_js.user_set.add(self.user_js)

    def test_register_with_auth(self):
        self.client.force_login(self.user_login)
        response = self.client.get(self.reg)
        self.assertEqual(response.status_code, 302)

    def test_POST_login_valid(self):
        response = self.client.post(self.login,
                                    {
                                        'username': 'belajar',
                                        'password ': "belajar1PPW"
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_login_invalid(self):
        response = self.client.post(self.login,
                                    {
                                        'username': '',
                                        'password ': ""
                                    }, follow=True)
        self.assertTemplateUsed(response, 'login/login.html')

    def test_POST_reg_valid(self):
        response = self.client.post(self.reg,
                                    {
                                        "username": "hahaha",
                                        "email": "haha@gmail.com",
                                        'password1': '123321hahaha',
                                        'password2': '123321hahaha',
                                        'typePekerjaan': 'Jobseeker',
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_reg_valid_vnp(self):
        response = self.client.post(self.reg,
                                    {
                                        "username": "hahaha",
                                        "email": "haha@gmail.com",
                                        'password1': '123321hahaha',
                                        'password2': '123321hahaha',
                                        'typePekerjaan': 'VolPerawat',
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_reg_invalid(self):
        response = self.client.post(self.reg,
                                    {
                                        "username": "hahaha",
                                        "email": "hagmail.com",
                                        'password1': '123321hahaha',
                                        'password2': '',
                                    }, follow=True)
        self.assertTemplateUsed(response, 'login/register.html')

    def test_POST_editProfile_js_not_first_time(self):
        self.client.force_login(self.user_js)
        pelamar = Jobseeker.objects.create(
            user=self.user_js,
            nama_pelamar="pencarikerja",
            umur_pelamar=17,
            provinsi_pelamar="1",
            kota_pelamar="Bogor",
            pendidikan_terakhir="MA",
        )
        response = self.client.post(self.edit, {
            'nama_pelamar': "pencarikerja",
            'umur_pelamar': 17,
            'provinsi_pelamar': "1",
            'kota_pelamar': "Bogor",
            "pendidikan_pelamar": "MA",
        }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_editProfile_js_first_time(self):
        self.client.force_login(self.user_js)
        response = self.client.post(self.edit, {
            'nama_pelamar': "Joni",
            'umur_pelamar': 17,
            'provinsi_pelamar': "1",
            'kota_pelamar': "Bogor",
            "pendidikan_pelamar": "MA",
        }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_GET_editProfile_not_first_time(self):
        pelamar = Jobseeker.objects.create(
            user=self.user_js,
            nama_pelamar="pencarikerja",
            umur_pelamar=17,
            provinsi_pelamar="1",
            kota_pelamar="Bogor",
            pendidikan_terakhir="MA",
        )
        self.client.force_login(self.user_js)
        response = self.client.get(self.edit)
        self.assertEqual(response.status_code, 200)

    def test_login_while_is_auth(self):
        self.client.force_login(self.user_js)
        response = self.client.get(self.login)
        self.assertEqual(response.status_code, 302)


class RegisterLoginTest(TestCase):
    def setUp(self):
        # Group.objects.create(name='RumahSakit')
        # rs_user = User.objects.create_user(username='rs1', password='123')
        # Group.objects.get(name='RumahSakit').user_set.add(rs_user)
        
        # rs = RS.objects.create(
        #     nama = "RSJ Test",
        #     alamat = "Grogol",
        #     jml_prwt = 1,
        #     jml_non = 1,
        #     user = rs_user
        # )
        # rs.save()
        
        # Group.objects.create(name='VolunteerNonPerawat')
        self.user = User.objects.create_user(username='testes', password='123')
        # Group.objects.get(name='VolunteerNonPerawat').user_set.add(self.user)

    def test_register_redirect(self):
        response = self.client.post(reverse('register'), {
            'username': 'testes',
            'email': 'cicakbinkadal@gmail.com',
            'password1': '123',
            'password2': '123',
            'typePekerjaan': 'VolNonPerawat',
        }, follow=True)
        self.assertRedirects(response, '/')

    def test_register_jobseeker(self):
        response = Client().post(reverse('register'), {
            'username': 'testes3',
            'email': 'cicakbinkadal3@gmail.com',
            'password1': '123',
            'password2': '123',
            'typePekerjaan': 'Jobseeker',
        }, follow=True)
        self.assertRedirects(response, '/login/editProfile/')

    def test_register_rumahsakit(self):
        response = Client().post(reverse('register'), {
            'username': 'testes3',
            'email': 'cicakbinkadal3@gmail.com',
            'password1': '123',
            'password2': '123',
            'typePekerjaan': 'RumahSakit',
        }, follow=True)
        self.assertRedirects(response, '/RumahSakit/daftarRS/')

    def test_register_vnp(self):
        response = Client().post(reverse('register'), {
            'username': 'testes3',
            'email': 'cicakbinkadal3@gmail.com',
            'password1': '123',
            'password2': '123',
            'typePekerjaan': 'VolNonPerawat',
        }, follow=True)
        self.assertRedirects(response, '/RumahSakit/listRS/')
    
    def test_register_perawat(self):
        response = Client().post(reverse('register'), {
            'username': 'testes3',
            'email': 'cicakbinkadal3@gmail.com',
            'password1': '123',
            'password2': '123',
            'typePekerjaan': 'VolPerawat',
        }, follow=True)
        self.assertRedirects(response, '/RumahSakit/listRS/')

    def test_register_invalid_type(self):
        response = Client().post(reverse('register'), {
            'username': 'testes3',
            'email': 'cicakbinkadal3@gmail.com',
            'password1': '123',
            'password2': '123',
            'typePekerjaan': 'Yoink',
        }, follow=True)
        self.assertRedirects(response, '/')

    def test_login_success(self):
        response = Client().post(reverse('login'), {
            'username': 'testes',
            'password': '123',
        })
        self.assertRedirects(response, '/')

    # def test_login_redirect_success(self):
    #     login = self.client.login(username='testes', password='123')
    #     response = self.client.get('/VolunteerNonPerawat/1', follow=True)
    #     self.assertRedirects(response, '/VolunteerNonPerawat/1')

    def test_logout(self):
        login = self.client.login(username='testes', password='123')
        response = self.client.post(reverse('logout'), {
            'username': 'testes',
            'password': '123',
        }, follow=True)
        self.assertRedirects(response, '/')

    def test_rlogout(self):
        login = self.client.login(username='testes', password='123')
        response = self.client.post(reverse('registerLogout'), {
            'username': 'testes',
            'password': '123',
        }, follow=True)
        self.assertRedirects(response, '/login/register/')
