from django.urls import path

from . import views

app_name = 'VolunteerNonPerawat'

urlpatterns = [
    path('<int:id>', views.index, name='index'),
    path('detail/<int:id>', views.detail_vnp, name='detail_vnp'),
    path('approve/<int:rumah_sakit_id>/<int:id>', views.approve_vnp, name='approve_vnp'),
    path('reject/<int:rumah_sakit_id>/<int:id>', views.reject_vnp, name='reject_vnp'),
    path('finish/<int:rumah_sakit_id>/<int:id>', views.finish_vnp, name='finish_vnp'),
]