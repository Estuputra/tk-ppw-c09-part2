# Generated by Django 3.1.3 on 2020-12-26 23:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('VolunteerNonPerawat', '0002_auto_20201227_0606'),
    ]

    operations = [
        migrations.AlterField(
            model_name='volunteernonperawat',
            name='no_telp',
            field=models.CharField(max_length=15, null=True),
        ),
    ]
