$(document).ready(function() {
    $('#no_telp-box').bind('keyup', function() {
        var char = this.value.charAt(0);     // Use vanilla JavaScript to get the
        var theRest = this.value.substring(1,this.value.length);
                                             // first character of the text field
        if (/[0-9]/.test(char)) {            // Test against a pattern: digit
            $('.registered_phonenum').val('+62' + theRest);
        }
        // } else if (/[a-zA-Z]/.test(char)) {  // Else, pattern: letters
        //     $(this).attr("name", "letter");
        // } else {                             // Finally, no name?
        //     $(this).attr("name", "");
        // }
    });

    // $('#tes').bind('click', function() {
    //     var char = $('#no_telp-box').value.charAt(0);     // Use vanilla JavaScript to get the
    //     var theRest = $('#no_telp-box').value.substring(1,this.value.length);
    //     $('.registered_phonenum').val('+62' + theRest);
    // });
});