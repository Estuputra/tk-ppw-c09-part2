from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from .forms import AddForm
from .models import VolunteerNonPerawat
from RumahSakit.models import RS
from django.contrib import messages
from django.contrib.auth.models import Group
# from django.core import serializers
from django.http import JsonResponse

choices = [
    ('1', 'Aceh'),
    ('2', 'Sumatra Utara'),
    ('3', 'Sumatra Barat'),
    ('4', 'Riau'),
    ('5', 'Kepulauan Riau'),
    ('6', 'Bengkulu'),
    ('7', 'Jambi'),
    ('8', 'Sumatra Selatan'),
    ('9', 'Bangka Belitung'),
    ('10', 'Lampung'),
    ('11', 'Banten'),
    ('12', 'Jawa Barat'),
    ('13', 'DKI Jakarta'),
    ('14', 'Jawa Tengah'),
    ('15', 'Yogyakarta'),
    ('16', 'Jawa Timur'),
    ('17', 'Bali'),
    ('18', 'NTB'),
    ('19', 'NTT'),
    ('20', 'Kalimantan Timur'),
    ('21', 'Kalimantan Barat'),
    ('22', 'Kalimantan Selatan'),
    ('23', 'Kalimantan Tengah'),
    ('24', 'Kalimantan Utara'),
    ('25', 'Gorontalo'),
    ('26', 'Sulawesi Barat'),
    ('27', 'Sulawesi Tengah'),
    ('28', 'Sulawesi Selatan'),
    ('29', 'Sulawesi Tenggara'),
    ('30', 'Sulawesi Utara'),
    ('31', 'Maluku'),
    ('32', 'Maluku Utara'),
    ('33', 'Papua Barat'),
    ('34', 'Papua'),
]
CHOICES_CLEANED = []
for i in choices:
    CHOICES_CLEANED.append(i[1])

@login_required(login_url="/login/login/")
def index(request, id):
    if Group.objects.get(name='VolunteerNonPerawat') in request.user.groups.all():
        # tidak bisa daftar ke >1 rumah sakit
        vnp_already_exists = False
        vnp_obj = None

        try:
            vnp_obj = VolunteerNonPerawat.objects.get(user=request.user)
            
            if vnp_obj.rumah_sakit.id == id:
                messages.error(request, f'Anda sudah terdaftar menjadi Volunteer di {vnp_obj.rumah_sakit.nama}')
                return redirect('/RumahSakit/listRS/')
        except ObjectDoesNotExist: # jika sudah registrasi akun VNP tapi blm daftar jadi VNP di add_vnp()
            pass
        # jika obj rumah_sakit nya null. Merupakan VNP yang sblmnya sudah daftar tapi direject / udah selesai (finished)
        except AttributeError:
            vnp_already_exists = True

        form = AddForm(request.POST)
        rumahsakit = RS.objects.get(id=id)

        if request.method == "POST":
            if (form.is_valid() and request.method == 'POST'): 
                data = form.cleaned_data
                try:
                    vnp = VolunteerNonPerawat.objects.get(user=request.user)
                    vnp.nama = data['nama']
                    vnp.umur = data['umur']
                    vnp.email = data['email']
                    vnp.provinsi = data['provinsi']
                    vnp.kota = data['kota']
                    vnp.no_telp = data['no_telp']
                    vnp.pengalaman = data['pengalaman']
                    vnp.alasan = data['alasan']
                    vnp.rumah_sakit = rumahsakit
                    vnp.user = request.user
                    vnp.save()
                except ObjectDoesNotExist:  
                    vnp = VolunteerNonPerawat()
                    vnp.nama = data['nama']
                    vnp.umur = data['umur']
                    vnp.email = data['email']
                    vnp.provinsi = data['provinsi']
                    vnp.kota = data['kota']
                    vnp.no_telp = data['no_telp']
                    vnp.pengalaman = data['pengalaman']
                    vnp.alasan = data['alasan']
                    vnp.rumah_sakit = rumahsakit
                    vnp.user = request.user
                    vnp.save()

                messages.success(request, f'Anda telah berhasil melamar menjadi VolunteerNonPerawat di {rumahsakit.nama}!')

                return redirect('/')
                
            messages.error(request, f'Terdapat kesalahan dalam mengisi form!')
        if (vnp_already_exists == True):
            return render(request, 'index.html', {'form' : form, 'listProvinsi': CHOICES_CLEANED, 'vnp_already_exists' : vnp_already_exists, 'vnp' : vnp_obj})
        
        return render(request, 'index.html', {'form' : form, 'listProvinsi': CHOICES_CLEANED, 'vnp_already_exists' : vnp_already_exists})

    messages.error(request, 'Akun Anda bukan merupakan akun VolunteerNonPerawat, sehingga tidak bisa melamar sebagai VolunteerNonPerawat')

    return redirect('/RumahSakit/listRS')

def detail_vnp(request, id):
    vnp = VolunteerNonPerawat.objects.get(id = id)
    dict_choices = dict(choices)
    
    return JsonResponse({
        'id' : vnp.id, 
        'nama' : vnp.nama, 
        'umur' : vnp.umur, 
        'provinsi' : dict_choices[vnp.provinsi], 
        'kota' : vnp.kota, 
        'pengalaman' : vnp.pengalaman, 
        'alasan' : vnp.alasan,
        'rumah_sakit_id' : vnp.rumah_sakit.id,
        'approval' : vnp.approval})

def approve_vnp(request, rumah_sakit_id, id):
    vnp = VolunteerNonPerawat.objects.get(id = id)
    rs = RS.objects.get(id = rumah_sakit_id)
    rs.jml_non -= 1
    vnp.approval = True
    vnp.save()
    rs.save()

    return JsonResponse({'nama' : vnp.nama})

def reject_vnp(request, rumah_sakit_id, id):
    vnp = VolunteerNonPerawat.objects.get(id = id)
    rs = RS.objects.get(id = rumah_sakit_id)
    vnp.approval = False
    vnp.rumah_sakit = None
    vnp.save()
    rs.save()

    return JsonResponse({'nama' : vnp.nama})

def finish_vnp(request, rumah_sakit_id, id):
    vnp = VolunteerNonPerawat.objects.get(id = id)
    rs = RS.objects.get(id = rumah_sakit_id)
    rs.jml_non += 1
    vnp.approval = False
    vnp.rumah_sakit = None
    vnp.save()
    rs.save()

    return JsonResponse({'nama' : vnp.nama})