from django.test import TestCase, Client
from django.apps import apps
from django.urls import reverse
from django.contrib.auth.models import User, Group
from .models import VolunteerNonPerawat, RS
from .apps import VolunteerNonPerawatConfig

class Test(TestCase):
    def setUp(self):
        Group.objects.create(name='RumahSakit')
        user_rs = User.objects.create_user(username='testuser', password='12345')
        rs = RS.objects.create(nama ='aaaa', alamat='Jonggol', jml_prwt = '2', jml_non = '2', user = user_rs)
        rs.save()

        Group.objects.create(name='VolunteerNonPerawat')
        self.user = User.objects.create_user(username='testuser2', password='12345')
        Group.objects.get(name='VolunteerNonPerawat').user_set.add(self.user)
        login = self.client.login(username='testuser2', password='12345')

    def test_urls(self):
        response = self.client.get(reverse('VolunteerNonPerawat:index', args=[1]))
        self.assertEqual(response.status_code, 200)

    def test_template_used(self):
        response = self.client.get(reverse('VolunteerNonPerawat:index', args=[1]))
        self.assertTemplateUsed(response, 'index.html')

    def test_create_object(self):
        vnp = VolunteerNonPerawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='+6281219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia', user = self.user)
        self.assertEquals(VolunteerNonPerawat.objects.all().count(), 1)

    def test_create_object_with_form(self):
        response = self.client.post(reverse('VolunteerNonPerawat:index', args=[1]), {'nama' : 'Budi', 'umur' : '19', 
            'email' : 'budibudiman@gmail.com', 'provinsi' : '13', 'kota' : 'Jakarta Timur', 'no_telp' : '+6281219593496',
            'pengalaman' : 'Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', 'alasan' : 'Ingin membantu sesama manusia', 'user' : self.user})
        vnp = VolunteerNonPerawat.objects.filter(nama='Budi').first()
        self.assertEquals(response.status_code, 302)
        self.assertEquals(VolunteerNonPerawat.objects.all().count(), 1)
    
    def test_daftar_vnp_jika_sebelumnya_sudah_sempat_melamar_dan_masih_terikat_dengan_rs(self):
        rs = RS.objects.get(id = 1)
        vnp = VolunteerNonPerawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='+6281219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia', user = self.user, rumah_sakit = rs)
        response = self.client.post(reverse('VolunteerNonPerawat:index', args=[1]), follow=True)
        
        self.assertRedirects(response, '/RumahSakit/listRS/')
    
    def test_daftar_vnp_jika_sebelumnya_sudah_sempat_melamar_rs_null(self):
        vnp = VolunteerNonPerawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='+6281219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia', user = self.user, rumah_sakit = None)
        response = self.client.post(reverse('VolunteerNonPerawat:index', args=[1]))
        
        self.assertEquals(response.context['vnp_already_exists'], True)
    
    def test_daftar_vnp_jika_sebelumnya_sudah_sempat_melamar_dan_belum_terikat_dengan_rs(self):
        vnp = VolunteerNonPerawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='+6281219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia', user = self.user, rumah_sakit = None)
        response = self.client.post(reverse('VolunteerNonPerawat:index', args=[1]), {'nama' : 'Budi', 'umur' : '19', 
            'email' : 'budibudiman@gmail.com', 'provinsi' : '13', 'kota' : 'Jakarta Timur', 'no_telp' : '+6281219593496',
            'pengalaman' : 'Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', 'alasan' : 'Ingin membantu sesama manusia', 'user' : self.user})
        
        self.assertRedirects(response, '/')
    
    def test_daftar_vnp_tapi_bukan_group_vnp(self):
        self.user = User.objects.create_user(username='testuser3', password='12345')
        login = self.client.login(username='testuser3', password='12345') 
        response = self.client.post(reverse('VolunteerNonPerawat:index', args=[1]), {'nama' : 'Budi', 'umur' : '19', 
            'email' : 'budibudiman@gmail.com', 'provinsi' : '13', 'kota' : 'Jakarta Timur', 'no_telp' : '+6281219593496',
            'pengalaman' : 'Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', 'alasan' : 'Ingin membantu sesama manusia', 'user' : self.user}, follow=True)
        
        self.assertRedirects(response, '/RumahSakit/listRS/')

    def test_detail_vnp(self):
        rs = RS.objects.get(id = 1)
        vnp = VolunteerNonPerawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='+6281219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia', user = self.user, rumah_sakit = rs)
        response = Client().get(reverse('VolunteerNonPerawat:detail_vnp', args=[1]))
        
        self.assertJSONEqual(response.content, {
            "id": 1, 
            "nama": "Budi", 
            "umur": 19, 
            "provinsi": "DKI Jakarta", 
            "kota": "Jakarta Timur", 
            "pengalaman": "Menjadi relawan kemanusiaan saat terjadi gempa di Lombok", 
            "alasan": "Ingin membantu sesama manusia", 
            "rumah_sakit_id": 1, 
            "approval": None})
    
    def test_approve_vnp(self):
        rs = RS.objects.get(id = 1)
        vnp = VolunteerNonPerawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='+6281219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia', user = self.user, rumah_sakit = rs)
        response = Client().get(reverse('VolunteerNonPerawat:approve_vnp', args=[1, 1]))

        self.assertJSONEqual(response.content, {"nama": "Budi"})
    
    def test_reject_vnp(self):
        rs = RS.objects.get(id = 1)
        vnp = VolunteerNonPerawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='+6281219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia', user = self.user, rumah_sakit = rs)
        response = Client().get(reverse('VolunteerNonPerawat:reject_vnp', args=[1, 1]))

        self.assertJSONEqual(response.content, {"nama": "Budi"})
    
    def test_finish_vnp(self):
        rs = RS.objects.get(id = 1)
        vnp = VolunteerNonPerawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='+6281219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia', user = self.user, rumah_sakit = rs)
        response = Client().get(reverse('VolunteerNonPerawat:finish_vnp', args=[1, 1]))

        self.assertJSONEqual(response.content, {"nama": "Budi"})

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(VolunteerNonPerawatConfig.name, 'VolunteerNonPerawat')
        self.assertEqual(apps.get_app_config('VolunteerNonPerawat').name, 'VolunteerNonPerawat')
