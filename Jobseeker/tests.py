from django.test import TestCase
from django.urls import resolve, reverse
from .models import Jobseeker
from .forms import FormPelamar
from .views import testFrontEnd
from django.test import Client
from .apps import JobseekerConfig
from django.apps import apps
from django.contrib.auth import authenticate, login, logout

from django.contrib.auth.models import User, Group

from Kantor.models import Kantor, KantorUser  # Tambahan buat Kantor
# Create your tests here.


class ModelTestJobseeker(TestCase):
    def setUp(self):
        self.userLogin = User.objects.create_user(
            username="halo", email='halo@gmail.com', password='123321123321haha')
        self.pelamar = Jobseeker.objects.create(
            user=self.userLogin,
            nama_pelamar="Joni",
            umur_pelamar=17,
            provinsi_pelamar="1",
            kota_pelamar="Bogor",
            pendidikan_terakhir="MA",
        )

    def test_instance_created(self):
        self.assertEqual(Jobseeker.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.pelamar), "Joni")


class FormTest(TestCase):
    def test_form_is_valid(self):
        form_jobseeker = FormPelamar(
            data={
                'nama_pelamar': "Joni",
                'umur_pelamar': 17,
                'provinsi_pelamar': "1",
                'kota_pelamar': "Bogor",
                'pendidikan_terakhir': "MA",
            }
        )
        self.assertTrue(form_jobseeker.is_valid())

    def test_form_invalid(self):
        form_jobseeker = FormPelamar(
            data={

            }
        )
        self.assertFalse(form_jobseeker.is_valid())


class UrlTest(TestCase):

    def test(self):
        pembuat_lowongan = KantorUser.objects.create(
            nama="PT Sejahtera",
            alamat="Kuburan",
            deskripsi_kantor="kantor"
        )

        kantor = Kantor.objects.create(
            Kantor=pembuat_lowongan,
            nama_pekerjaan="PM",
            alamat="jalancinta",
            tipe_pekerjaan="Fulltime",
        )
        kantor.save()
        found = resolve(reverse('daftar', args=[1]))
        self.assertTrue(found.func == testFrontEnd)


class ViewTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.pembuat_kerja = User.objects.create_user(
            username="sibos", email='oi@gmail.com', password='123321123321haha')
        self.group_js = Group.objects.create(name="Jobseeker")
        self.kantor_user = KantorUser.objects.create(
            user=self.pembuat_kerja, nama='ptpt')
        self.pekerjaan = Kantor.objects.create(Kantor=self.kantor_user, nama_pekerjaan="PM",
                                               alamat="jalancinta",
                                               tipe_pekerjaan="Fulltime",
                                               )
        self.user_js = User.objects.create_user(
            username='pencarikerja', password='123')
        self.user_js_galengkap = User.objects.create_user(
            username='pencarikerjagalengkap', password='123')
        self.pelamar = Jobseeker.objects.create(
            user=self.user_js,
            nama_pelamar="pencarikerja",
            umur_pelamar=17,
            provinsi_pelamar="1",
            kota_pelamar="Bogor",
            pendidikan_terakhir="MA",
        )
        self.pelamar_galengkap = Jobseeker.objects.create(
            user=self.user_js_galengkap,
            nama_pelamar="pencarikerjawwww",
            umur_pelamar=17,
            provinsi_pelamar="",
            kota_pelamar="",
            pendidikan_terakhir="",
        )
        self.group_js.user_set.add(self.user_js)
        self.listDilamar = reverse('pekerjaanDidaftar')
        self.ajaxFTime = reverse('ajaxFtime')
        self.ajaxPTime = reverse('ajaxPtime')
        self.ajaxIntern = reverse('ajaxIntern')
        self.daftarKerja = reverse('daftar', args=[1])

    def test_daftar_gawe_get_belum_lengkap(self):
        self.client.force_login(self.user_js_galengkap)
        response = self.client.get(self.daftarKerja)
        self.assertEqual(response.status_code, 200)

    def test_daftar_gawe_get_lengkap(self):
        self.client.force_login(self.user_js)
        response = self.client.get(self.daftarKerja)
        self.assertEqual(response.status_code, 200)

    def test_ajax_ftime(self):
        self.client.force_login(self.user_js)
        response = self.client.get(self.ajaxFTime)
        self.assertTrue(response.status_code, 200)

    def test_ajax_Ptime(self):
        self.client.force_login(self.user_js)
        response = self.client.get(self.ajaxPTime)
        self.assertTrue(response.status_code, 200)

    def test_ajax_intern(self):
        self.client.force_login(self.user_js)
        response = self.client.get(self.ajaxIntern)
        self.assertTrue(response.status_code, 200)

    def test_allowed(self):
        self.client.logout()
        self.client.force_login(self.user_js)
        response = self.client.get(self.listDilamar)
        self.assertTrue(response.status_code, 200)

    def test_dissalowed(self):
        anon = User.objects.create_user(
            username="oi", email='oi@gmail.com', password='123321123321haha')
        response = Client().get('/Jobseeker/listDilamar')
        self.assertTemplateUsed('Jobseeker/gagal.html')

# test


class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(JobseekerConfig.name, 'Jobseeker')
        self.assertEqual(apps.get_app_config('Jobseeker').name, 'Jobseeker')
