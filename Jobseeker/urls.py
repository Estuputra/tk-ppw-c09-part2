from django.urls import path

from . import views
urlpatterns = [
    path('daftar/P<int:job_id>', views.testFrontEnd, name='daftar'),
    path('listDilamar', views.listDilamar, name='pekerjaanDidaftar'),
    path('listDilamar/ajaxFtime', views.ajaxFtime, name='ajaxFtime'),
    path('listDilamar/ajaxPtime', views.ajaxPtime, name='ajaxPtime'),
    path('listDilamar/ajaxIntern', views.ajaxIntern, name='ajaxIntern'),

]
