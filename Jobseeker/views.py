from django.core import serializers
from django.contrib.auth.models import User
from django.shortcuts import render, HttpResponse, redirect
from .forms import FormPelamar
from django.http import JsonResponse
from .models import Jobseeker
from Kantor.models import Kantor
from django.contrib.auth.models import Group


# Create your views here.


choices = [
    ('1', 'Aceh'),
    ('2', 'Sumatera Utara'),
    ('3', 'Sumatera Barat'),
    ('4', 'Riau'),
    ('5', 'Kepulauan Riau'),
    ('6', 'Bengkulu'),
    ('7', 'Jambi'),
    ('8', 'Sumatera Selatan'),
    ('9', 'Kepulauan Bangka Belitung'),
    ('10', 'Lampung'),
    ('11', 'Banten'),
    ('12', 'Jawa Barat'),
    ('13', 'Dki Jakarta'),
    ('14', 'Jawa Tengah'),
    ('15', 'Di Yogyakarta'),
    ('16', 'Jawa Timur'),
    ('17', 'Bali'),
    ('18', 'Nusa Tenggara Barat'),
    ('19', 'Nusa Tenggara Timur'),
    ('20', 'Kalimantan Timur'),
    ('21', 'Kalimantan Barat'),
    ('22', 'Kalimantan Selatan'),
    ('23', 'Kalimantan Tengah'),
    ('24', 'Kalimantan Utara'),
    ('25', 'Gorontalo'),
    ('26', 'Sulawesi Barat'),
    ('27', 'Sulawesi Tengah'),
    ('28', 'Sulawesi Selatan'),
    ('29', 'Sulawesi Tenggara'),
    ('30', 'Sulawesi Utara'),
    ('31', 'Maluku'),
    ('32', 'Maluku Utara'),
    ('33', 'Papua Barat'),
    ('34', 'Papua'),
]
CHOICES_CLEANED = []
for i in choices:
    CHOICES_CLEANED.append(i[1])


def listDilamar(request):
    passed_group1 = Group.objects.get(name="Jobseeker")
    # passed_group2 => ntar ini buat group buat Kantor yaaa ichsan

    if passed_group1 in request.user.groups.all():
        context = {}
        all_pekerjaan = Kantor.objects.all()
        listCleared = []
        for i in all_pekerjaan:
            if i in Jobseeker.objects.get(nama_pelamar=request.user.username).pekerjaan_dilamar.all():
                listCleared.append(i)

        context['pekerjaans'] = listCleared

        return render(request, 'Jobseeker/listDilamar.html', context)
    else:
        return render(request, 'gagal.html', {'allowed': 'Jobseeker '})


def ajaxFtime(request):
    print()
    return JsonResponse(serializers.serialize('json', Jobseeker.objects.get(
        nama_pelamar=request.user.username).pekerjaan_dilamar.filter(tipe_pekerjaan="Fulltime")), safe=False)


def ajaxPtime(request):
    return JsonResponse(serializers.serialize('json', Jobseeker.objects.get(
        nama_pelamar=request.user.username).pekerjaan_dilamar.filter(tipe_pekerjaan="Part time")), safe=False)


def ajaxIntern(request):
    return JsonResponse(serializers.serialize('json', Jobseeker.objects.get(
        nama_pelamar=request.user.username).pekerjaan_dilamar.filter(tipe_pekerjaan="Internship")), safe=False)


def testFrontEnd(request, job_id):
    pelamar = Jobseeker.objects.get(user=request.user)
    kantor_dilamar = Kantor.objects.get(pk=job_id)
    if request.method == "POST":
        Jobseeker.objects.get(
            user=request.user).pekerjaan_dilamar.add(kantor_dilamar)
        return JsonResponse(serializers.serialize('json', Jobseeker.objects.filter(
            user=request.user)), safe=False)
    else:
        print(pelamar.umur_pelamar == None)
        if pelamar.umur_pelamar == None or pelamar.provinsi_pelamar == None or pelamar.kota_pelamar == None or pelamar.pendidikan_terakhir == None:
            return redirect('/login/editProfile')
        return render(request, 'Jobseeker/melamar.html', {'listProvinsi': CHOICES_CLEANED, 'pekerjaan': kantor_dilamar})
