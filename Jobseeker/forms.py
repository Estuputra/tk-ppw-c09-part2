from django import forms


class FormPelamar(forms.Form):

    nama_pelamar = forms.CharField(
        label="Nama",
        max_length=64,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
            }
        )
    )

    umur_pelamar = forms.IntegerField(
        label="Umur",
        widget=forms.NumberInput(
            attrs={
                'class': 'form-control',
                'min': '17',
                'max': '55',
                'id': 'umur_pelamar'
            }
        )
    )

    kota_pelamar = forms.CharField(
        label="Asal Kota",
        max_length=200,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'id': 'kota_pelamar'

            }
        )
    )

    pendidikan_terakhir = forms.ChoiceField(
        label='Last Experience',
        widget=forms.RadioSelect(
            attrs={
                'id': 'pendidikan_pelamar',
                'class': 'pendidikan'

            }
        ),
        choices=[
            ('MP', 'SMP'),
            ('MA', 'SMA'),
            ('S1', 'Sarjana'),
            ('S2', 'Pascasarjana'),
        ]

    )
