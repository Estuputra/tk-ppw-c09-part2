from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User
from Kantor.models import Kantor
# Create your models here.


class Jobseeker(models.Model):
    LAST_ED = (
        ('MA', 'SMA'),
        ('S1', 'Sarjana'),
        ('S2', 'Pascasarjana'),
    )

    PROVINSI = (
        ('1', 'Aceh'),
        ('2', 'Sumatera Utara'),
        ('3', 'Sumatera Barat'),
        ('4', 'Riau'),
        ('5', 'Kepulauan Riau'),
        ('6', 'Bengkulu'),
        ('7', 'Jambi'),
        ('8', 'Sumatera Selatan'),
        ('9', 'Bangka Belitung'),
        ('10', 'Lampung'),
        ('11', 'Banten'),
        ('12', 'Jawa Barat'),
        ('13', 'Jakarta'),
        ('14', 'Jawa Tengah'),
        ('15', 'Di Yogyakarta'),
        ('16', 'Jawa Timur'),
        ('17', 'Bali'),
        ('18', 'Nusa Tenggara Barat'),
        ('19', 'Nusa Tenggara Timur'),
        ('20', 'Kalimantan Timur'),
        ('21', 'Kalimantan Barat'),
        ('22', 'Kalimantan Selatan'),
        ('23', 'Kalimantan Tengah'),
        ('24', 'Kalimantan Utara'),
        ('25', 'Gorontalo'),
        ('26', 'Sulawesi Barat'),
        ('27', 'Sulawesi Tengah'),
        ('28', 'Sulawesi Selatan'),
        ('29', 'Sulawesi Tenggara'),
        ('30', 'Sulawesi Utara'),
        ('31', 'Maluku'),
        ('32', 'Maluku Utara'),
        ('33', 'Papua Barat'),
        ('34', 'Papua'),
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nama_pelamar = models.CharField(max_length=64)
    umur_pelamar = models.IntegerField(
        validators=[
            MaxValueValidator(55),
            MinValueValidator(17)
        ], blank=True, null=True)
    pekerjaan_dilamar = models.ManyToManyField(
        Kantor, related_name='pelamar')
    provinsi_pelamar = models.CharField(
        max_length=2, choices=PROVINSI, blank=True)
    kota_pelamar = models.CharField(max_length=15, blank=True)
    pendidikan_terakhir = models.CharField(
        max_length=2, choices=LAST_ED, blank=True)

    def __str__(self):
        return self.nama_pelamar
