$(document).ready(function() {
    // Implemented by Farhan Ahmad Primaditya (for VolunteerNonPerawat)
    var overlay_rs = $('.overlay');
    var tr_rs_list = $('.isi_rs');

    overlay_rs.hide();
    $('.overlay_vnp').hide();

    // Saat hover ke RS
    // tr_rs_list.bind('mouseover', function() {
    //     if (overlay_rs.css("display") == "none") {
    //         addVNPList($('.rsId').text());
    //     }
    // });

    // Saat keluar dari hover RS
    tr_rs_list.bind('mouseleave', function() {
        if (overlay_rs.hasClass('active').toString() == "true") {
            if (overlay_rs.hasClass('toggle').toString() == "false") {
                overlay_rs.removeClass("active");
                overlay_rs.hide("fast");
            }
        }
    });

    // Utk toggle overlay RS
    tr_rs_list.bind('click', function() {
        if (overlay_rs.hasClass('toggle').toString() != "true") {
            overlay_rs.addClass("toggle");
        }
        else {
            overlay_rs.removeClass("toggle");
        }
    });
});

// function tes() {
//     console.log("halo");
// }

function addVNPList(id) {
    $.ajax({
        type: 'GET',
        url: '/RumahSakit/ListPendaftarVNP/' + id,
        dataType: 'json',
        success: function(response) {
            console.log(response);

            // block.insertAfter(block.next());

            // var table = $('.overlay > td > table');
            // table.empty();
            // table.append("<tbody><tr><th>VolunteerNonPerawat</th></tr>");

            // for (var i = 0; i < response.length; i++) {
            //     var res = "<tr style=\"text-align:'center';\" class=\"text-sm-center isi_vnp\">";
            //     res += "<td><a onclick=showDetailVNP(" + response[i].pk + ") style=\"color: blue\" class=link-web href=";
            //     // res += "/RumahSakit/ListPendaftar/" + id + ">";
            //     res += "#>";
            //     res += response[i].fields.nama + "</td></tr>";

            //     table.append(res);
            // } 
            
            // table.append("</tbody>");

            // $('.overlay').show("fast");
            // $('.overlay').addClass("active")
        }
    });
}

function showDetailVNP(id) {
    $.ajax({
        type: 'GET',
        url: '/VolunteerNonPerawat/detail/' + id,
        dataType: 'json',
        success: function(response) {
            // console.log(response);

            var overlay = $('.overlay_vnp');

            overlay.empty();

            var res = "";
            res += "<strong class=mb-2>Nama : " + response.nama + "</strong>";
            res += "<div class=mb-2>Umur : " + response.umur + "</div>";
            res += "<div class=mb-2>Kota, Provinsi : " + response.kota + ", " + response.provinsi + "</div>";
            res += "<div class=mb-2>Pengalaman : " + response.pengalaman + "</div>";
            res += "<div class=mb-2>Alasan : " + response.alasan + "</div>";
            res += "<div class=\"d-flex flex-row\">"

            if (response.approval == true) {
                res += "<button onclick=finishVNP(" + response.id + "," + response.rumah_sakit_id + ") class=mr-2>Finish</button>";
            }
            else {
                res += "<button onclick=acceptVNP(" + response.id + "," + response.rumah_sakit_id + ") class=mr-2>Accept</button>";
                res += "<button onclick=rejectVNP(" + response.id + "," + response.rumah_sakit_id + ")>Reject</button>";
            }
            
            res += "</div>"

            overlay.append(res)

            overlay.show();
            // $('.overlay').addClass("active")
        }
    });
}

function acceptVNP(id, rumah_sakit_id) {
    if (confirm('Anda yakin ingin menerima lamaran NonPerawat ini?')) {
        $.ajax({
            type: 'GET',
            url: "/VolunteerNonPerawat/approve/" + rumah_sakit_id + "/" + id,
            dataType: 'json',
            success: function (response) {
                alert('Anda telah menerima lamaran NonPelamar ' + response.nama);
                window.location = '/RumahSakit/listRS/';
            },
        });
    }
}

function rejectVNP(id, rumah_sakit_id) {
    if (confirm('Anda yakin ingin menolak lamaran NonPerawat ini?')) {
        $.ajax({
            type: 'GET',
            url: "/VolunteerNonPerawat/reject/" + rumah_sakit_id + "/" + id,
            dataType: 'json',
            success: function (response) {
                alert('Anda telah menolak lamaran NonPelamar ' + response.nama);
                window.location = '/RumahSakit/listRS/';
            },
        });
    }
}

function finishVNP(id, rumah_sakit_id) {
    if (confirm('Anda yakin ingin menyelesaikan status volunteer NonPerawat ini?')) {
        $.ajax({
            type: 'GET',
            url: "/VolunteerNonPerawat/finish/" + rumah_sakit_id + "/" + id,
            dataType: 'json',
            success: function (response) {
                alert('NonPerawat ' + response.nama + ' telah selesai menjadi volunteer di Rumah Sakit anda');
                window.location = '/RumahSakit/listRS/';
            },
        });
    }
}