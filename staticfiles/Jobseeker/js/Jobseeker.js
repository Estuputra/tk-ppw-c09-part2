
    $(document).ready(function () {
        $('#btnJobseeker').on('click', function (e) {
            e.preventDefault();
            if (validator()) {
                ;
            } else {
                $.ajax({
                    type: 'POST',
                    url: "/login/editProfile/",
                    data: {
                        nama_pelamar: $('#id_nama_pelamar').val(),
                        umur_pelamar: $('#umur_pelamar').val(),
                        pendidikan_pelamar: document.querySelector('input[name="pendidikan_terakhir"]:checked').getAttribute('value'),
                        kota_pelamar: $('#kota_pelamar').val(),
                        provinsi_pelamar: $('#provinsi_pelamar').val(),
                        csrfmiddlewaretoken: $('input[name = csrfmiddlewaretoken]').val(),
                    },
                    success: function (response) {
                        console.log(response)
                        $('.alert').css('opacity', '1')
                        $('#provinsi_pelamar_conf').append(response.value_prov)
                        $('#kota_pelamar_conf').append(response.kota_pelamar)
                        $('#last_ed_conf').append(response.pendidikan_pelamar)
                        $('#umur_pelamar_conf').append(response.umur_pelamar)

                    },
                    beforeSend: function () {

                        $('#provinsi_pelamar_conf').empty()
                        $('#provinsi_pelamar_conf').append('Provinsi pelamar: ')
                        $('#kota_pelamar_conf').empty()
                        $('#kota_pelamar_conf').append('Kota pelamar: ')
                        $('#last_ed_conf').empty()
                        $('#last_ed_conf').append('Pendidikan terakhir pelamar: ')
                        $('#umur_pelamar_conf').empty()
                        $('#umur_pelamar_conf').append('Umur pelamar: ')

                    }
                });
            }
        });
        function validator() {
            if ($('#umur_pelamar').val() < 17 || $('#umur_pelamar').val() > 55 || toString($('#umur_pelamar').val()) == "") {
                alert('please input valid age! (17 - 55)')
                return true;
            }

            if ($('#kota_pelamar').val() == "") {
                alert('please fill the blank!')
                return true;
            }


            var option = document.getElementsByName('pendidikan_terakhir');
            if (!(option[0].checked || option[1].checked || option[2].checked || option[3].checked)) {
                alert('please pick the last education!')
                return true;
            }
        }
        var close = document.getElementsByClassName("closebtn");
        var i;

        for (i = 0; i < close.length; i++) {
            close[i].onclick = function () {
                var div = this.parentElement;
                div.style.opacity = "0";

            }
        }

        let arr = []

        $.ajax({
            url: 'https://dev.farizdotid.com/api/daerahindonesia/provinsi',
            type: 'GET',
            async: false,
            success: function (response) {
                //alert($('#provinsi_pelamar option:selected').html().trim())
                response.provinsi.map((e) => {

                    if (e.nama == $('#provinsi_pelamar option:selected').html().trim()) {
                        $.ajax({
                            url: 'https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=' + e.id,
                            type: 'GET',
                            success: function (response) {
                                response.kota_kabupaten.map((k) => {
                                    arr.push(k.nama.replace('Kota', '').replace('Kabupaten', '').trim())
                                })
                            }
                        })
                    }
                }
                )
            },
        })


        $('#provinsi_pelamar').on('change', function () {
            $.ajax({
                url: 'https://dev.farizdotid.com/api/daerahindonesia/provinsi',
                type: 'GET',
                async: false,
                success: function (response) {
                    //alert($('#provinsi_pelamar option:selected').html().trim())
                    response.provinsi.map((e) => {

                        if (e.nama == $('#provinsi_pelamar option:selected').html().trim()) {
                            $.ajax({
                                url: 'https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=' + e.id,
                                type: 'GET',
                                success: function (response) {
                                    while (arr.length) { arr.pop(); }
                                    response.kota_kabupaten.map((k) => {
                                        arr.push(k.nama.replace('Kota', '').replace('Kabupaten', '').trim())
                                    })
                                }
                            })
                        }
                    }
                    )
                },
            })
        })

        console.log(arr)

        /*Code from w3*/
        function autocomplete(inp, arr) {
            var currentFocus;

            inp.on("input", function (e) {
                var a, b, i, val = this.value;
                closeAllLists();
                if (!val) { return false; }
                currentFocus = -1;
                a = document.createElement("DIV");
                a.setAttribute("id", this.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");
                this.parentNode.appendChild(a);
                for (i = 0; i < arr.length; i++) {

                    if (arr[i].substr(0, val.length).toUpperCase().includes(val.toUpperCase())) {
                        b = document.createElement("DIV");
                        b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                        b.innerHTML += arr[i].substr(val.length);
                        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                        b.addEventListener("click", function (e) {
                            $('#kota_pelamar').val(this.getElementsByTagName("input")[0].value);
                            closeAllLists();
                        });
                        a.appendChild(b);
                    }
                }
            });

            inp.on("keydown", function (e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                    currentFocus++;
                    addActive(x);
                } else if (e.keyCode == 38) { //up
                    currentFocus--;
                    addActive(x);
                } else if (e.keyCode == 13) {
                    e.preventDefault();
                    if (currentFocus > -1) {
                        if (x) x[currentFocus].click();
                    }
                }
            });


            function addActive(x) {
                if (!x) return false;
                removeActive(x);
                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
                x[currentFocus].classList.add("autocomplete-active");
            }

            function removeActive(x) {
                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
            }

            function closeAllLists(elmnt) {
                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != inp) {
                        x[i].parentNode.removeChild(x[i]);
                    }
                }
            }

            document.addEventListener("click", function (e) {
                closeAllLists(e.target);
            });
        }
        autocomplete($('#kota_pelamar'), arr);
    });
  
